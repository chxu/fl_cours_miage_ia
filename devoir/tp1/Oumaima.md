**Nom**: AMAZIANE

**Prénom**: Oumaima

**Date**:

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** :
- Le jeu de données testé est CIFAR10 

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :
- data_split est "iid" donc l'ensemble d'entrainement est divisé en num_clients partitions de manière équitable. C'est à dire la liste de proportions est calculée en divisant équitablement 1 par le nombre de clients (1/num_clients). Cela garantit que chaque client reçoit une part égale des données.
- On peut trouver cette information dans la fonction load_dataset à l'interieur de la condition if data_split.
- Chaque client utilise 22500 pour l'entrainement et 2500 pour la validation


### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** :
- Le modèle testé est un réseau de neurones convolutif (CNN).


2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :
- Le modèle a l'architecture suivante:
    * Couche de convolution : 3 canaux d'entrée, 6 canaux de sortie, taille du noyau 5x5
    * Couche de pooling : max pooling avec une taille de noyau 2x2
    * Couche de convolution : 6 canaux d'entrée, 16 canaux de sortie, taille du noyau 5x5
    * Couche entièrement connectée : 16 * 5 * 5 neurones en entrée, 120 neurones en sortie
    * Couche entièrement connectée : 120 neurones en entrée, 84 neurones en sortie
    * Couche de sortie : 84 neurones en entrée, 10 neurones en sortie


### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :
- l'algorithme: FedAvg cette information est peut etre trouvée dans stratégie de server.py


2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

- Nombre d'époques est 1, et il est précisé comme valeur par défaut dans Get node id de client.py

    **Réponse** :
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
- 10 tours de communication, et elle est précisé comme valeur par défaut de --round dans server.py

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** :
- chaque client a une méthode evaluate qui est utilisée pour évaluer le modèle sur son propre ensemble de validation. Cette méthode est appelée par le serveur lors de chaque tour de communication pour obtenir les métriques d'évaluation du modèle actuel.
- 10% pour l'évaluation et 90% pour l'entrainement

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
- Cette information peut etre trouvée dans le fonction weighted_average de server.

3. Quelle est la précision finale du modèle ?

    **Réponse** :
- l'accuracy est egale: 0.6289999999999999
## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python

    ## prepare_dataset.py:
    elif data_split == "non_iid_number":
        props = np.random.dirichlet([alpha]*num_clients)
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))

    ## client.py:
    parser.add_argument(
        "--alpha",
        type=float,
        default=0.8,
        help="alpha",
    )


      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
   - pour le premier client: 17765
   - pour le deuxième client: 32235
       
      **Réponse** :
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** :
  - la précision finale est : 0.6826722338204593 
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :
- les données sont divisées entre les clients de manière à ce que chaque client possède un sous-ensemble spécifique de classes. Les échantillons appartenant à une classe particulière sont attribués à des clients distincts, créant ainsi une distribution non-identique des classes entre les clients. Cette approche simule un scénario de non-IID où les clients ont des ensembles de données locaux avec des distributions de classes différentes.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** :
- les performance ont chuté considérablement jusqu'a 0.475

  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
    - Oui l'execution est plus rapide parceque ya que 5 rounds de communication entre le client et le serveur
        
       **Réponse** :
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
    - cest toujours des résultats mals une précision égale à 0.4762

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
    - oui c'est plus rapide, parceque y'a que 2 rounds de communication
        
       **Réponse** :
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
    - la performance du modele a chuté à 0.3892

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       # prepare_dataset.py

       def load_datasets(num_clients: int, alpha:float,data_split: str, dataset):
        transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        )
        if dataset == "CIFAR10":
            trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
            testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
        if dataset == "CIFAR100":
            trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
            testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
        else:
            raise NotImplementedError(
              "The dataset is not implemented"
            )

      
   
       ```