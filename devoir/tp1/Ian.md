**Nom**:

**Prénom**:

**Date**:

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : 
   Le jeu de données utilisé est cifar-10.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : 
    Les données sont réparties entre les clients de manière non-iid.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** :
    Le modèle utilisé est un réseau de neurones convolutif (CNN).

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :
    Le modèle comporte 3 couches de convolution, 3 couches de max pooling, 2 couches de dropout, 2 couches de fully connected et 1 couche de sortie.

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : 
    L'algorithme utilisé pour l'entraînement local est la descente de gradient stochastique (SGD).
    
2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : 
    Le nombre d'époques en fixé à 1 par défaut.
    On trouve cette information au niveau de l'argument --local_epochs dans le fichier client.py.

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
    Le nombre de tours de communication est fixé à 10 par défaut.
    On trouve cette information au niveau de l'argument --rounds dans le fichier server.py.

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** :
    L'évaluation est réalisée par le serveur.
    22500 pour l'entraînement et 2500 pour la validation.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
    On trouve cette information dans la fonction `weighted_average` dans le fichier `server.py`.

3. Quelle est la précision finale du modèle ?

    **Réponse** :
    La précision finale du modèle est de 0.6198.


## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    # To add in prepare_dataset.py :
    elif data_split == "non_iid_number":
        alpha = 0.8

        props = torch.tensor(np.random.dirichlet([alpha] * num_clients))
        total_size = len(trainset)
        indexes = torch.randperm(total_size)

        partition_sizes = [int(prop * total_size) for prop in props]
        partition_sizes[-1] = total_size - sum(partition_sizes[:-1])

        datasets = []
        start_index = 0
        for size in partition_sizes:
            end_index = start_index + size
            datasets.append(Subset(trainset, indexes[start_index:end_index]))
            start_index = end_index
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** :
      On a 34454 données d'entraînement et 3828 de validation.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** :
      La précision finale avec 0.8 en alpha est de 0.6101.
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :
      Les données sont distribuées de manière non-iid selon les classes. Chaque client se voit assigner un ensemble de classes spécifiques et ses données sont limitées à ces classes. 
      Cette approche garantit que chaque client a un ensemble unique de classes dans ses données.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** :
     La précision finale du modèle avec alpha=0.8 est légèrement inférieure à celle des résultats précédents. Cela peut indiquer que l'augmetation du niveau de non-iidness des données, contrôlée par alpha, peut avoir un impact négatif sur les performances du modèle.
     Les différence dans le nombre d'images entre les clients sont plus importantes, ce qui peut conduire à une convergence plus lente du modèle global pendant l'entraînement.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
        L'exécution est plus rapide grâce à la combinaison d'un nombre de round d'entraînement réduit et d'une augmentation du nombre d'époques de calcul local. Ces changements permettent une convergence plus rapide du modèle global pendant l'entraînement.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
       La réduction du nombre de round d'entraînement et l'augmentation du nombre d'époques de calcul local peuvent entrainer une convergence plus rapide du modèle, mais cela peut égalment conduire à une précision finale légèrement inférieure et une stabilité de formation réduite.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
       L'exécution est plus rapide avec la combinaison d'un nombre de round d'entraînement réduit et une augmentation du nombre d'époques de calcul local. Ces changements permettent une convergence plus rapide du modèle global pendant l'entraînement.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
       Malgré une augmentation significative du nombre d'époques de calcul local, le modèle n'a pas réussi à atteindre une précision aussi élevée que dans les cas précédents. Le modèle pourrait nécessiter plus d'itérations pour converger vers une solution optimale.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       # to add in prepare_dataset load_datasets function
       elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)

       # to edit in client.py

       self.fc3 = nn.Linear(84, 100) # in Net init

       # change CIFAR10 by CIFAR100 in argument --dataset
       ```