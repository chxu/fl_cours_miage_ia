**Nom**: SAEZ

**Prénom**: Meriem

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le jeu de données testé est CIFAR10

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : Les données sont réparties de manière identiquement indépendante (iid) entre les clients. Cette information se trouve dans les arguments du script (--data_split="iid"), et la répartition spécifique est gérée par la fonction get_data_loader dans le fichier prepare_dataset.py.
Le jeu de donnéescontient 50,000 images d'entraînement, sachant qu'il ya  deux clients alors un client utilise 22500 images

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** :Un modèle de réseau de neurones convolutif (CNN) a été testé

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :
Le modèle comprend 2 couches convolutives suivies de 3 couches entièrement connectées :
-Convolution 1: Conv2d avec entrée de 3 canaux, sortie de 6 canaux, noyau de taille 5
-Convolution 2: Conv2d avec entrée de 6 canaux, sortie de 16 canaux, noyau de taille 5
-Entièrement connecté 1: 16 * 5 * 5 entrées, 120 sorties
-Entièrement connecté 2: 120 entrées, 84 sorties
-Entièrement connecté 3: 84 entrées, 10 sorties

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :L'algorithme d'optimisation utilisé est SGD et on le trouve dans la fonction train du fichier client.py

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Chaque client effectue le nombre d'époques spécifié par l'argument --local_epochs, qui est défini par défaut à 1. On peut trovuer cette information à partir des arguments du script (local_epochs).

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :Il y a 10 tours de communication au total comment il est marqué dans le fichier server.py où c'est dit dans --round que par défaut c'est 10:
"--round",
type=int,
default=10,

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation du modèle dans le cadre du federated learning est réalisée par les clients. Chaque client évalue le modèle sur son propre ensemble de données de validation et renvoie les résultats au serveur pour une agrégation. Dans le cas de deux clients avec une répartition IID des données CIFAR10, chaque client utilise 2,500 images pour l'évaluation. Ces informations sont déterminées par le script `prepare_dataset.py` et la logique d'évaluation est implémentée dans `client.py`

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?
Dans la fonction weighted_average
try:
    with open('log.txt', 'a') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy) + " ")
except FileNotFoundError:
    with open('log.txt', 'w') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy) + " ")


3. Quelle est la précision finale du modèle ?

    **Réponse** :La précision finale du modèle, enregistrée dans le fichier log.txt, est de 63.72%.



## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
``` elif data_split == "non_iid_number":
        alpha = 0.5  # Ajustez cette valeur en fonction du niveau de non-IID souhaité
        proportions = np.random.dirichlet([alpha] * num_clients)
        proportions *= len(trainset)
        proportions = np.round(proportions).astype(int)
        indices = torch.randperm(len(trainset))
        datasets = []
        start = 0
        for i in range(num_clients):
            end = start + proportions[i]
            datasets.append(Subset(trainset, indices[start:end]))
            start = end
      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** :[17055, 32945]
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.5907092334161728
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :Les données sont distribuées parmi les clients basées sur les classes dans le mode non_iid_class. Chaque client reçoit un ensemble de classes spécifiques, avec le nombre total de classes divisé également entre les clients. Cela signifie que si vous avez 10 classes et 2 clients, chaque client pourrait se voir attribuer 5 classes différentes, rendant la distribution des données non identiquement distribuée (non-IID) entre eux. Cette répartition est conçue pour simuler des scénarios réalistes où différents clients peuvent avoir des données très différentes.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** :La diminution de la précision du modèle, passant d'environ 60% à 50%, peut indiquer que le modèle apprend moins efficacement à partir des données non-IID par rapport aux données IID. La répartition non-IID, où chaque client reçoit des données représentant un sous-ensemble spécifique de classes, peut limiter la diversité des données vues par le modèle pendant l'entraînement, rendant plus difficile la généralisation à de nouvelles données. Cela souligne l'importance de la stratégie de répartition des données dans l'apprentissage fédéré et son impact significatif sur la performance du modèle.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapidE ? Pourquoi ? 

Mettre rounds à 5 et pourrait rendre l'exécution plus rapide en termes de temps total écoulé, car il y aurait moins de communications entre les clients et le serveur. Cependant, augmenter le nombre d'époques locales signifie que chaque client passe plus de temps à entraîner le modèle sur ses données locales. L'effet net sur la rapidité d'exécution dépend de l'équilibre entre la réduction du temps passé en communication et l'augmentation du temps d'entraînement local.
   
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :

L'acuracy a diminuée à 38.42% avec ces nouveaux paramètres donc le modèle souffre d'une généralisation insuffisante ou d'un surapprentissage sur les données non-IID fournies à chaque client.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
Réduire les rounds à 2 et augmenter les local_epochs à 5 peut accélérer l'exécution en termes de temps réel car il y a moins d'interactions réseau entre les clients et le serveur. Cependant, chaque client effectuera plus de travail localement en raison du nombre accru d'époques. Cela pourrait réduire le temps global si la communication réseau est un facteur limitant majeur, mais l'augmentation du temps de calcul local pourrait aussi compenser les gains de temps.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :La baisse de précision à 37.52% avec les paramètres round = 2 et local_epochs = 5 montre que les performances sont moins bonnes. Cela peut suggérer que le modèle ne bénéficie pas suffisamment des mises à jour globales et pourrait être davantage sujet au surapprentissage sur les jeux de données locaux non-IID. Cette configuration accentue le défi de l'apprentissage fédéré avec des données non-IID, où moins d'itérations globales limitent l'opportunité pour le modèle d'apprendre de manière globale.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       # dans le fichier prepare_dataset.py j'ai rajouté :
    elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)

puis dans le client.py j'ai rajouté ces conditions dans la classe Net dans __init__
        if dataset == "CIFAR10":
            self.fc3 = nn.Linear(84, 10)  # Pour CIFAR10
        elif dataset == "CIFAR100":
            self.fc3 = nn.Linear(84, 100)
      
   
       ```