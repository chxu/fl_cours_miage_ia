**Nom**:

**Prénom**:

**Date**:

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le jeu de données testé est CIFAR10, comme indiqué par le paramètre --dataset par défaut dans client.py.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : Les données sont réparties de manière iid (identiquement et indépendamment distribuées) entre les clients par défaut, comme indiqué par le paramètre --data_split par défaut dans client.py. La répartition spécifique des données entre les clients est gérée dans prepare_dataset.py par la fonction get_data_loader. La répartition est faite de manière à diviser le jeu de données d'entraînement en partitions égales pour chaque client et à réserver 10 % de chaque partition pour la validation.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** :n réseau de neurones convolutif simple (Net) a été testé, comprenant des couches de convolution, des couches de pooling, et des couches entièrement connectées.

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle comporte 5 couches principales :

  2 couches de convolution (conv1 et conv2),
  3 couches entièrement connectées (fc1, fc2, fc3).

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :L'algorithme de descente de gradient stochastique (SGD) avec un taux d'apprentissage de 0.01 et un momentum de 0.9 est utilisé, comme indiqué dans la fonction train de client.py.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** :Le nombre d'époques locales (local_epochs) est défini par un paramètre, avec une valeur par défaut de 1. Cela est spécifié dans client.py.

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :Le nombre total de tours de communication (num_rounds) est défini par un paramètre dans server.py, avec une valeur par défaut de 10.

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** :L'évaluation est réalisée par les clients. Chaque client évalue le modèle sur son jeu de données de validation local et renvoie les résultats au serveur pour l'agrégation.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :L'enregistrement des résultats d'évaluation dans "log.txt" est effectué dans la fonction weighted_average de server.py.

3. Quelle est la précision finale du modèle ?

    **Réponse** : 0.636

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    #(Veuillez copie-coller votre code ici 
      elif data_split == "non_iid_number":
        alpha = 0.8
        num_samples = len(trainset)
        dirichlet_distribution = np.random.dirichlet([alpha] * num_clients)
        num_samples_per_client = [int(p * num_samples) for p in dirichlet_distribution]
        num_samples_per_client[-1] = num_samples - sum(num_samples_per_client[:-1])
        datasets = []
        index = 0
        for num_samples in num_samples_per_client:
            indices = list(range(index, index + num_samples))
            datasets.append(Subset(trainset, indices))
            index += num_samples
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : 22500
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.6142
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : les données sont distribuées parmi les clients de manière à ce que chaque client reçoive un sous-ensemble de données concentré sur certaines classes spécifiques. Cela signifie que la distribution des classes n'est pas équitablement répartie entre tous les clients, ce qui entraîne une situation où différents clients s'entraînent sur des segments distincts du jeu de données total.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : la précision a baissé en moyenne.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Oui, Réduire le nombre de rounds tout en augmentant légèrement le nombre d'époques locales peut réduire le temps total d'exécution en diminuant le temps perdu en communication, même si chaque client effectue plus de travail localement.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :Avec moins de rounds et plus d'époques locales, chaque client a plus d'opportunités d'apprendre de ses données locales, ce qui a amélioré la performance locale de chaque modèle client avant l'agrégation, et donc un gain de temps. Cependant, avec moins d'itérations d'agrégation (rounds), il y a moins d'occasions pour le modèle global d'apprendre de l'ensemble diversifié des clients, ce qui a réduit la généralisation du modèle global sur des données non vues, et donc une perte de précision.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :L'exécution est encore plus rapide en termes de temps total d'exécution par rapport à la configuration initiale, car il y a encore moins de rounds de communication. Augmenter le nombre d'époques locales à 5 permet un apprentissage local plus approfondi avec encore moins d'interruptions pour la communication.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Bien que chaque client puisse potentiellement mieux apprendre à partir de son jeu de données local en raison de plus d'époques d'entraînement, l'agrégation très limitée peut entraîner un modèle global qui ne reflète pas efficacement l'apprentissage combiné de tous les clients. Cela va résulter en une performance moindre sur des données globales ou diversifiées

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       # Veuillez copie-coller votre changement de code ici avec le nom de fichier
      prepare_dataset.py :
      from torchvision.datasets import CIFAR100

      def load_datasets(num_clients: int, dataset, data_split):
        # Download and transform CIFAR-10 (train and test)
        transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        )
        if dataset == "CIFAR10":
            trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
            testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
        elif dataset == "CIFAR100":
            trainset = CIFAR100(root='./data', train=True, download=True, transform=transform)
            testset = CIFAR100(root='./data', train=False, download=True, transform=transform)
        elif data_split == "non_iid_number":
            alpha = 0.8
            num_samples = len(trainset)
            dirichlet_distribution = np.random.dirichlet([alpha] * num_clients)
            num_samples_per_client = [int(p * num_samples) for p in dirichlet_distribution]
            num_samples_per_client[-1] = num_samples - sum(num_samples_per_client[:-1])
            datasets = []
            index = 0
            for num_samples in num_samples_per_client:
                indices = list(range(index, index + num_samples))
                datasets.append(Subset(trainset, indices))
                index += num_samples
        else:
            raise NotImplementedError(
              "The dataset is not implemented"
            )

      [...] (reste du code)

      client.py:
      self.fc3 = nn.Linear(84, 100)

      [...]
      
      parser.add_argument(
        "--dataset",
        type=str,
        choices=["CIFAR10", "CIFAR100"],
        default="CIFAR100",
        help="Dataset: [CIFAR10]",

)
   
       ```