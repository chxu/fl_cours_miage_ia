**Nom**: BAILI

**Prénom**: Yannis

**Date**: 19/02/2024

# TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
```bash
  > python server.py
  ```
* Dans Terminal 2:
```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le jeu de données testé est CIFAR-10.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
   **Réponse** : Les données peuvent être réparties de deux manières : IID (identiquement distribuées) ou non-IID. Cette information est déterminée par l'argument data_split dans le fichier prepare_dataset.py. Chaque client utilise une partition de données locale pour l'entraînement et la validation. Le nombre d'images utilisées par chaque client est déterminé par la fonction get_data_loader dans prepare_dataset.py.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Le modèle testé est un réseau de neurones convolutionnel (CNN).

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :Le modèle comporte plusieurs couches :
                  Couche de convolution avec 6 filtres, suivie d'une couche de pooling.
                  Une autre couche de convolution avec 16 filtres, suivie d'une autre couche de pooling.
                  Deux couches entièrement connectées (ou linéaires) avec des activations ReLU entre elles.
                  Une couche de sortie avec 10 neurones (pour les 10 classes de CIFAR-10).

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

   **Réponse** : Chaque client applique l'algorithme de descente de gradient stochastique (SGD) pour l'entraînement du modèle local. Cette information est implémentée dans la fonction train dans le fichier client.py.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

   **Réponse** : Le nombre d'époques est défini par l'argument local_epochs dans le fichier client.py. La valeur par défaut est 1.

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est défini par l'argument rounds dans le fichier server.py. Il y a 10 rounds.

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

   **Réponse** : L'évaluation est réalisée par les clients. Les tailles des jeux de données pour l'évaluation sont les tailles des ensembles de validation locaux de chaque client. La taille est égal à 10% du dataset.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

   **Réponse** : Les résultats de l'évaluation sont enregistrés dans le fichier log.txt. Cette opération est effectuée dans la fonction weighted_average du fichier server.py.

3. Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale du modèle est d'environ 62,42%.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

   **Réponse** :
   ```python
      # prepare_dataset.py

        import torch
        import torchvision.transforms as transforms
        from torch.utils.data import DataLoader, random_split, Subset
        from torchvision.datasets import CIFAR10
        import numpy as np

        def load_datasets(num_clients: int, dataset, data_split, alpha=0.1):
            # Download and transform CIFAR-10 (train and test)
            transform = transforms.Compose(
                [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
            )
            if dataset == "CIFAR10":
                trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
                testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
            else:
                raise NotImplementedError("The dataset is not implemented")

            if data_split == "iid":
                # Split training set into `num_clients` partitions to simulate different local datasets
                props = [1/num_clients]*num_clients
                datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
            elif data_split == "non_iid_number":
                # Distribute the data based on a Dirichlet distribution
                num_examples = len(trainset)
                dirichlet_samples = np.random.dirichlet([alpha]*num_clients)
                num_samples_per_client = (dirichlet_samples * num_examples).astype(int)
                indices = torch.randperm(num_examples).tolist()
                datasets = []
                for num_samples in num_samples_per_client:
                    client_indices = indices[:num_samples]
                    datasets.append(Subset(trainset, client_indices))
                    indices = indices[num_samples:]
            elif data_split == "non_iid_class":
                num_classes = len(trainset.classes)
                class_per_client = num_classes/num_clients
                data_indices_each_client = {client: [] for client in range(num_clients)}
                for c in range(num_classes):
                    indices = (torch.tensor(trainset.targets)[..., None] == c).any(-1).nonzero(as_tuple=True)[0]
                    client_belong = int(c/class_per_client)
                    data_indices_each_client[client_belong].extend(list(indices))
                datasets = []
                for i in range(num_clients):
                    datasets.append(Subset(trainset, data_indices_each_client[i]))
            else:
                raise NotImplementedError("The data split is not implemented")

            # Split each partition into train/val and create DataLoader
            trainloaders = []
            valloaders = []
            for ds in datasets:
                len_val = len(ds) // 10  # 10 % validation set
                len_train = len(ds) - len_val
                lengths = [len_train, len_val]
                ds_train, ds_val = random_split(ds, lengths, torch.Generator().manual_seed(42))
                trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
                valloaders.append(DataLoader(ds_val, batch_size=32))
            testloader = DataLoader(testset, batch_size=32)
            return trainloaders, valloaders, testloader


        def get_data_loader(num_clients: int, cid: int, dataset="CIFAR10", data_split="iid", alpha=0.1):
            trainloaders, valloaders, testloader = load_datasets(num_clients, dataset, data_split, alpha)
            return trainloaders[cid], valloaders[cid], testloader   
   ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : Entrainement = 1306 / Evaluation = 146.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : La précision finale est d'environ 61.76%.
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   

      **Réponse** : Dans le fichier prepare_dataset.py, la distribution des données entre les clients est déterminée par l'argument data_split passé à la fonction load_datasets. Dans ce cas, l'argument data_split est défini sur non_iid_class, ce qui signifie que les données sont distribuées entre les clients de manière non-identique en fonction des classes. Cela signifie que chaque client reçoit un sous-ensemble de classes spécifiques pour l'entraînement et la validation.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : En comparaison avec les résultats précédents, on observe une précision initiale plus faible du modèle (environ 30.48% au lieu de 43.02%). Cela peut être dû à la distribution non-identique des données entre les clients, ce qui peut rendre l'apprentissage plus difficile car certains clients peuvent recevoir des classes plus difficiles à distinguer que d'autres. Cela souligne l'importance de la gestion de la distribution des données lors de l'apprentissage fédéré, car elle peut avoir un impact significatif sur les performances du modèle.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapide ? Pourquoi ? 
        
       **Réponse** : Oui, l'exécution semble être plus rapide car le nombre d'époques de calcul local a été augmenté à 2 dans cette exécution par rapport aux exécutions précédentes qui avaient seulement 1 époque. Cela signifie que chaque client effectue maintenant deux époques d'entraînement local avant de communiquer avec le serveur pour agréger les mises à jour des modèles. En conséquence, le modèle peut converger plus rapidement vers une meilleure performance après chaque tour d'apprentissage fédéré.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

       **Réponse** : Comparé aux résultats précédents, l'ajout d'une époque d'entraînement local semble avoir conduit à une amélioration des performances globales du modèle. Cela suggère que l'entraînement pendant une période plus longue sur les données locales avant l'agrégation des mises à jour des modèles peut aider à améliorer la performance du modèle global. Cependant, il est également important de noter qu'il peut y avoir des limites à l'amélioration des performances en augmentant simplement le nombre d'époques d'entraînement local, car cela peut également conduire à un surapprentissage ou à une augmentation du temps de calcul.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapide ? Pourquoi ? 
        
       **Réponse** : Dans ce scénario, l'exécution est plus rapide en termes de nombre de tours d'apprentissage fédéré (--round 2). Cela signifie que seulement deux tours d'apprentissage fédéré sont exécutés, ce qui réduit le nombre total d'itérations de formation et d'évaluation. Par conséquent, l'exécution est plus rapide car moins de tours sont nécessaires pour compléter le processus.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

       **Réponse** : Par rapport aux résultats précédents, il semble que le modèle ait une performance moins bonne dans ce scénario. Les précisions observées après deux tours sont plus faibles (environ 29.42% et 36.98%) par rapport aux autres exécutions où plus de tours ont été effectués. Cela pourrait indiquer que le modèle n'a pas eu suffisamment d'itérations pour converger vers une solution optimale, ce qui peut entraîner une performance moins bonne. Augmenter le nombre d'époques d'entraînement local peut également conduire à du surapprentissage, surtout si les données ne sont pas suffisamment diverses ou si le modèle est trop complexe. 

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fournie par torchvision)

       **Réponse** :
       ```python
       # prepare_dataset.py

      import torch
      import torchvision.transforms as transforms
      from torch.utils.data import DataLoader, random_split, Subset
      from torchvision.datasets import CIFAR10, CIFAR100
      import numpy as np

      def load_datasets(num_clients: int, dataset, data_split, alpha=0.1):
          # Download and transform CIFAR dataset
          transform = transforms.Compose(
              [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
          )
          if dataset == "CIFAR10":
              trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
              testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
          elif dataset == "CIFAR100":
              trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
              testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
          else:
              raise NotImplementedError("The dataset is not implemented")

          if data_split == "iid":
              # Split training set into `num_clients` partitions to simulate different local datasets
              props = [1/num_clients]*num_clients
              datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
          elif data_split == "non_iid_number":
              # Distribute the data based on a Dirichlet distribution
              num_examples = len(trainset)
              dirichlet_samples = np.random.dirichlet([alpha]*num_clients)
              num_samples_per_client = (dirichlet_samples * num_examples).astype(int)
              indices = torch.randperm(num_examples).tolist()
              datasets = []
              for num_samples in num_samples_per_client:
                  client_indices = indices[:num_samples]
                  datasets.append(Subset(trainset, client_indices))
                  indices = indices[num_samples:]
          elif data_split == "non_iid_class":
              num_classes = len(trainset.classes)
              class_per_client = num_classes/num_clients
              data_indices_each_client = {client: [] for client in range(num_clients)}
              for c in range(num_classes):
                  indices = (torch.tensor(trainset.targets)[..., None] == c).any(-1).nonzero(as_tuple=True)[0]
                  client_belong = int(c/class_per_client)
                  data_indices_each_client[client_belong].extend(list(indices))
              datasets = []
              for i in range(num_clients):
                  datasets.append(Subset(trainset, data_indices_each_client[i]))
          else:
              raise NotImplementedError("The data split is not implemented")

          # Split each partition into train/val and create DataLoader
          trainloaders = []
          valloaders = []
          for ds in datasets:
              len_val = len(ds) // 10  # 10 % validation set
              len_train = len(ds) - len_val
              lengths = [len_train, len_val]
              ds_train, ds_val = random_split(ds, lengths, torch.Generator().manual_seed(42))
              trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
              valloaders.append(DataLoader(ds_val, batch_size=32))
          testloader = DataLoader(testset, batch_size=32)
          return trainloaders, valloaders, testloader


      def get_data_loader(num_clients: int, cid: int, dataset="CIFAR10", data_split="iid", alpha=0.1):
          trainloaders, valloaders, testloader = load_datasets(num_clients, dataset, data_split, alpha)
          return trainloaders[cid], valloaders[cid], testloader   
       ```