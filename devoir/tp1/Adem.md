**Nom**: BEN JABRIA

**Prénom**: Adem

**Date**: rendu le 20/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le projet utilise le jeu de données **CIFAR-10** pour tester un modèle dans un cadre d'apprentissage fédéré. 

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?

    **Réponse** :  Les données sont réparties de manière **iid** entre les clients, comme indiqué par l'argument data_split="iid" dans prepare_dataset.py. Ce script divise également le jeu de données pour l'entraînement et la validation, où chaque client reçoit une portion équitable pour l'entraînement et 10% de cette portion est utilisée pour la validation.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Un réseau de neurones convolutif (CNN) est implémenté dans la classe Net du fichier client.py. Cette architecture est choisie pour sa capacité à extraire des caractéristiques spatiales hiérarchiques des images, adaptée à la tâche de classification d'images de CIFAR-10.
   
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle comporte des couches de convolution, de pooling, et entièrement connectées, détaillées dans client.py. Il commence par des couches de convolution pour l'extraction de caractéristiques, suivies de couches de pooling pour la réduction de dimensionnalité, et se termine par des couches entièrement connectées pour la classification.

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : L'algorithme SGD est utilisé pour l'entraînement, avec un taux d'apprentissage de 0.01 et un momentum de 0.9, comme configuré dans la fonction train de client.py. Cette méthode est privilégiée pour sa simplicité et son efficacité dans l'optimisation des réseaux de neurones.
    
2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Chaque client effectue une époque de calcul local par défaut, paramètre contrôlé par l'argument --local_epochs dans client.py. Cette configuration reflète un compromis entre la précision de l'apprentissage et la charge computationnelle.
    
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre de tours de communication est fixé à 10, comme indiqué par l'argument --round dans server.py. Ce paramètre détermine le nombre d'itérations d'apprentissage et de mise à jour entre les clients et le serveur.

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** : Les clients évaluent le modèle sur leurs ensembles de validation locaux, et les résultats sont agrégés par le serveur. Cette approche permet d'obtenir une mesure précise de la performance du modèle sur différentes distributions de données.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Les performances d'évaluation sont consignées dans le fichier "log.txt", comme orchestré par la fonction weighted_average dans server.py. Ce fichier sert de journal pour suivre l'évolution de la précision du modèle au fil des tours.

3. Quelle est la précision finale du modèle ?

    **Réponse** : 
0.4202 0.4832 0.5366 0.5574 0.5674 0.5736 0.5876 0.5944 0.6088 **0.622**

0.4186 0.4986 0.522 0.5496 0.5956 0.575 0.6016 0.6124 0.6082 **0.6102**

la précision finale du modèle est d'environ 61%

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
```python
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split, Subset
from torchvision.datasets import CIFAR10
import numpy as np

def load_datasets(num_clients: int, dataset, data_split, alpha=0.8): 
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset is not implemented")

    if data_split == "iid":
        props = [1 / num_clients] * num_clients
        datasets = random_split(trainset, [int(len(trainset) * p) for p in props], torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        proportions = np.random.dirichlet([alpha] * num_clients)
        # Calculez le nombre exact d'images pour chaque client en fonction des proportions
        num_images_per_client = [int(proportion * len(trainset)) for proportion in proportions]
        # Ajustez le dernier pour s'assurer que la somme est égale à la taille totale du dataset
        num_images_per_client[-1] = len(trainset) - sum(num_images_per_client[:-1])
        datasets = []
        start_index = 0
        for num_images in num_images_per_client:
            end_index = start_index + num_images
            indices = list(range(start_index, end_index))
            datasets.append(Subset(trainset, indices))
            start_index = end_index
    elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)
        class_per_client = num_classes/num_clients
        data_indices_each_client = {client: [] for client in range(num_clients)}
        for c in range(num_classes):
            indices = (torch.tensor(trainset.targets)[..., None] == c).any(-1).nonzero(as_tuple=True)[0]
            client_belong = int(c/class_per_client)
            data_indices_each_client[client_belong].extend(list(indices))
        datasets = []
        for i in range(num_clients):
            datasets.append(Subset(trainset, data_indices_each_client[i]))
    else:
        raise NotImplementedError(
           "The data split is not implemented"
        )

    trainloaders = []
    valloaders = []
    for ds in datasets:
        len_val = len(ds) // 10  
        len_train = len(ds) - len_val
        lengths = [len_train, len_val]
        ds_train, ds_val = random_split(ds, lengths, torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)

    return trainloaders, valloaders, testloader

 ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : Pour une division IID avec 5 clients, chaque client recevra exactement 10,000 images pour l'entraînement, étant donné que le dataset CIFAR-10 est divisé uniformément.
Pour une division non-IID basée sur la loi de Dirichlet avec un alpha de 0.8, les tailles des jeux de données pour chaque client peuvent varier considérablement, illustrant la distribution non uniforme des données. Dans cet exemple, les tailles sont les suivantes :
Client 1 : 11,065 images
Client 2 : 22,816 images
Client 3 : 2,603 images
Client 4 : 3,405 images
Client 5 : 10,111 images
Cela montre comment, dans une configuration non-IID, certains clients peuvent avoir beaucoup plus de données que d'autres, ce qui peut affecter l'apprentissage et la performance globale du modèle fédéré.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.4316 0.5076 0.542 0.552 0.5968 0.5994 0.606 0.626 0.6236 **0.6346** la précision finale du modèle est de 63.4%
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Lorsque l'option --data_split non_iid_class est utilisée, les données du dataset CIFAR-10 sont distribuées entre les clients de manière non identiquement distribuée (non-IID) basée sur les classes. Chaque client reçoit des données exclusivement d'un sous-ensemble spécifique de classes, divisant ainsi le dataset total en parties qui se concentrent chacune sur différentes classes. Par exemple, avec 2 clients et le dataset CIFAR-10 qui a 10 classes, chaque client pourrait se voir attribuer des images de 5 classes distinctes, assurant ainsi que chaque client entraîne son modèle sur un ensemble de données unique qui ne se chevauche pas en termes de classes avec celui d'autres clients.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** :  0.313 0.3616 0.395 0.4272 0.4832 0.492 0.5014 0.5068 0.5094 0.5126 
Les nouveaux résultats avec distribution non-IID montrent une progression plus lente et une performance finale inférieure par rapport aux résultats précédents, indiquant que la distribution non-IID des données par classe rend l'apprentissage initial plus difficile et limite la performance globale du modèle.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : L'exécution peut être plus rapide en termes de temps total grâce à la réduction du nombre de rounds, malgré des epochs locaux plus nombreux, car la diminution des communications réseau peut compenser l'augmentation du calcul local.
       
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : La modification à 5 rounds et 2 epochs locaux montre une amélioration rapide initiale mais plafonne plus tôt, indiquant que plus d'epochs locaux accélèrent l'apprentissage initial mais peuvent limiter l'amélioration globale faute de suffisamment d'agrégations.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Avec les horaires de début et de fin pour chaque lancement dans les logs on observe :
Avec local_epochs=1 round = 10  :
Début : 20:50:17.165088
Fin : 20:52:23.971
Durée : Environ 2 minutes et 7 secondes.

Avec local_epochs=2 round = 5  :
Début : 21:43:42.053142
Fin : 21:45:16.200
Durée : Environ 1 minute et 34 secondes.

Avec local_epochs=5 round = 2 :
Début : 21:52:06.197714
Fin : 21:53:45.699
Durée : Environ 1 minute et 39 secondes.
L'exécution avec 5 epochs locaux et 2 rounds est légèrement plus rapide comparée à la configuration avec 10 rounds. 
Cela s'explique par la réduction significative des communications réseau dues à moins de rounds, malgré un travail de calcul local accru.
Cependant, aucune différence significative n'a été observé avec le round 5.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
**Réponse** : Avec 2 rounds et 5 epochs locaux, la progression initiale du modèle (0.3096 à 0.3846) est plus modeste comparée aux configurations précédentes. Cela suggère une adaptation rapide mais limitée, avec potentiellement moins d'opportunité pour le modèle d'apprendre de la diversité globale des données à travers les agrégations fédérées.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

**Réponse** :
prepare_dataset.py
```python
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split, Subset
from torchvision.datasets import CIFAR10, CIFAR100
import numpy as np

def load_datasets(num_clients: int, dataset: str, data_split: str, alpha: float = 0.5):
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )

    # Chargement du dataset spécifié
    if dataset == "CIFAR10":
        trainset = CIFAR10(root="./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10(root="./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":
        trainset = CIFAR100(root="./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100(root="./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset is not implemented")

    # Division du dataset en fonction de data_split
    if data_split == "iid":
        props = [1 / num_clients] * num_clients
        datasets = random_split(trainset, [int(len(trainset) * p) for p in props], generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        proportions = np.random.dirichlet([alpha] * num_clients)
        num_images_per_client = [int(proportion * len(trainset)) for proportion in proportions]
        num_images_per_client[-1] = len(trainset) - sum(num_images_per_client[:-1])
        datasets = []
        start_index = 0
        for num_images in num_images_per_client:
            end_index = start_index + num_images
            datasets.append(Subset(trainset, list(range(start_index, end_index))))
            start_index = end_index
    elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)
        class_per_client = num_classes / num_clients
        data_indices_each_client = {client: [] for client in range(num_clients)}
        for c in range(num_classes):
            indices = (torch.tensor(trainset.targets) == c).nonzero(as_tuple=False).view(-1)
            client_belong = int(c // class_per_client)
            data_indices_each_client[client_belong].extend(indices.tolist())
        datasets = [Subset(trainset, indices) for indices in data_indices_each_client.values()]
    else:
        raise NotImplementedError("The data split is not implemented")

    # Création des DataLoaders pour l'entraînement et la validation
    trainloaders, valloaders = [], []
    for ds in datasets:
        len_val = len(ds) // 10  
        len_train = len(ds) - len_val
        ds_train, ds_val = random_split(ds, [len_train, len_val], generator=torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)

    return trainloaders, valloaders, testloader

def get_data_loader(num_clients: int, cid: int, dataset="CIFAR100", data_split="iid", alpha=0.5):
    trainloaders, valloaders, testloader = load_datasets(num_clients, dataset, data_split, alpha)
    return trainloaders[cid], valloaders[cid], testloader

```