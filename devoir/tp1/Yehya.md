**Nom**: YEHYA

**Prénom**: Abdel Rahim 

**Date**: 20/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : CIFAR10

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : le jeu de données est divisé de manière uniforme entre les "clients" , on peut trouver cette information dans le fichier
    "prepare_dataset.py" dans la methode "load_datasets". 704 images pour l'entrainement et 79 de validation (pour chaque client).

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : c'est un reseau de neuronnes convolutif (CNN)
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Il comporte une couche d'entree convolutive 2d qui accepte des images RGB (3 canaux) , cree 6 cartes de caracteristiques et utilise un noyau 5x5, suivi par une couche max pooling 2d pour reduire la dimensionalite, ensuite une autre couche convolutive 2d (6,16,5), et finalement 2 couches denses
   suivies par une derniere couche dense de sortie avec 10 classes. 

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : chaque client applique l'algorithme de descente de gradient stochastique (SGD) pour entraîner le modèle localement.Cette information peut être trouvée dans la fonction "train" définie dans le fichier client.py.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : le nombre d'époques de calcul local est spécifié dans la variable local_epochs, qui est définie à l'aide de l'argument de ligne de commande --local_epochs. Par defaut cette valeur est mise a "1". Cette information se trouve dans le fichier "client.py".
    
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est déterminé par la variable rounds, qui est spécifiée à l'aide de l'argument de ligne de commande --round. Par defaut la nombre de tours est 10 . Cette information se trouve dans le fichier "server.py".

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** :  l'évaluation est réalisée par les clients. Chaque client évalue localement le modèle sur son propre jeu de données de validation. Les clients envoient ensuite leurs résultats d'évaluation au serveur, qui agrège les résultats pour obtenir une évaluation globale. La taille de jeu des donnees d'evaluation est celle des donnees de validation de chaque client.

2. Les résultats de l'évaluation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : cette information se trouve dans la fonction "weighted_average" du fichier "server.py".

3. Quelle est la précision finale du modèle ?

    **Réponse** : la precision finale du modele est : 0.6192.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet).
    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    total_samples = len(trainset)
    alpha = 0.1 
    proportions = np.random.dirichlet([alpha] * num_clients)
    num_samples = (proportions * total_samples).astype(int)

    datasets = []
    for i in range(num_clients):
        indices = np.random.choice(total_samples, num_samples[i], replace=False)
        datasets.append(Subset(trainset, indices))
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : client0 : 299 , client1 : 203
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.55
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : ce code distribue les données entre les clients de manière à ce que chaque client traite un ensemble de classes différent, en fonction de la répartition des classes dans le jeu de données d'origine.(5 classes pour chaque client)

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : presque les memes resultat , l'accuracy finale : 0.52
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Non le temps d'execution n'a pas change , car d'un part on a reduit le nb de tour a moitie , et d'autre part on a double le nb d'epoques locals donc au final chaque client fait 10 epoques, mais on a reduit les tours de communications ce qui peut nous faire gagner du temps mais comme on travaille sur la meme machine , le temps de communications n'est pas considerable.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : les memes resultats  0.52

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : l'accuracy a diminue : 0.3868

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       from torchvision.datasets import CIFAR10, CIFAR100
       elif dataset == "CIFAR100":
          trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
          testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
       ```