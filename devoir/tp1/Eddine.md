**Nom**: JAMAL EDDINE

**Prénom**: SAAD

**Date**: 20/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : CIFAR-10

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
  **Réponse** : D'après prepare_dataset.py, les données sont distribuées en mode IID, où chaque client reçoit une part égale du dataset, garantissant une diversité similaire à travers tous les clients. En mode Non-IID par Classe, chaque client se concentre sur des images de certaines classes uniquement, créant des jeux de données locaux spécialisés et variés entre les clients.

La logique de repartition des donnees est implementée dans la fonction "load_datasets" .Cette fonction divise le jeu de données CIFAR-10 en fonction du paramètre data_split, qui peut être iid ou non_iid_class (la répartition non-IID basée sur le nombre n'est pas implémentée).

chaque client utilise 704 x 32 pour l'entrainement et 79 x 32 pour l'evaluation.


### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Le modèle testé est un réseau de neurones convolutif (CNN)

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle neural utilisé dans le fichier client.py est un réseau de neurones convolutif (CNN) qui comporte plusieurs couches. Voici le détail de sa structure :

Couche de convolution (conv1) : Cette couche utilise des filtres de taille 5x5 pour traiter l'entrée avec 3 canaux (couleurs RGB) et produit 6 canaux de sortie. Elle est suivie d'une fonction d'activation ReLU.

Pooling (pool) : Une couche de max pooling avec un noyau de 2x2 et un pas de 2. Cette couche est utilisée après chaque couche de convolution pour réduire la dimensionnalité des caractéristiques spatiales.

Couche de convolution (conv2) : Une deuxième couche convolutive qui prend les 6 canaux d'entrée de la précédente opération de pooling et produit 16 canaux de sortie, avec des filtres de taille 5x5, suivie également par une fonction d'activation ReLU et une opération de pooling.

Première couche entièrement connectée (fc1) : Cette couche linéaire transforme les caractéristiques extraites par les couches convolutives et de pooling en un vecteur de taille 120. Avant cette couche, les caractéristiques sont aplaties (flattened).

Deuxième couche entièrement connectée (fc2) : Prend les sorties de la première couche entièrement connectée et les transforme en un vecteur de taille 84.

Troisième couche entièrement connectée (fc3) : La dernière couche du réseau qui prend les sorties de la couche précédente et produit 10 sorties, correspondant au nombre de classes dans le CIFAR-10.



### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

  **Réponse** : Pour l'entraînement du modèle local sur chaque client, l'algorithme appliqué est le Gradient Descent Stochastique (SGD) avec momentum.
Dans la fonction train du fichier client.py, l'optimiseur est défini comme suit : optimizer = torch.optim.SGD(net.parameters(), lr=0.01, momentum=0.9)
Cela indique que chaque client utilise l'optimiseur SGD avec un taux d'apprentissage (lr) de 0.01 et un momentum de 0.9 pour mettre à jour les poids du modèle local pendant l'entraînement. Le momentum est utilisé pour accélérer l'optimisation en direction des minima et pour réduire l'oscillation.



2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

  **Réponse** : Le nombre d'époques de calcul local effectuées pour chaque client est défini par le paramètre --local_epochs dans le fichier client.py et par defaut c'est 1. " parser.add_argument("--local_epochs", type=int, default=1, help="époques") "



3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est défini par le paramètre --round dans le fichier server.py, et c'est 10. " parser.add_argument("--round", type=int, default=10, help="Partition of the dataset divided into 3 iid partitions created artificially.") "


### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation est réalisée par les clients. Chaque client évalue le modèle sur son propre ensemble de données de validation local et envoie les résultats de cette évaluation au serveur. Le serveur agrège ensuite ces résultats pour obtenir une mesure globale de la performance du modèle.
    La taille des jeux de donnees pour l'evaluation c'est 10% du dataset.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

  **Réponse** : Cette information est mentionnée dans le fichier server.py, spécifiquement dans la fonction qui gère l'agrégation des métriques d'évaluation envoyées par les clients au serveur "weighted_average": 
  "try:
    with open('log.txt', 'a') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy)+" ")
except FileNotFoundError:
    with open('log.txt', 'w') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy)+" ")"


3. Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale du modèle est de 0.6164.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
import numpy as np

def load_datasets(num_clients: int, dataset, data_split, alpha=0.5):  # Ajout du paramètre alpha avec une valeur par défaut
    # Download and transform CIFAR-10 (train and test)
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset is not implemented")

    if data_split == "iid":
        # Split training set into `num_clients` partitions to simulate different local datasets
        props = [1 / num_clients] * num_clients
        datasets = random_split(trainset, [int(len(trainset) * p) for p in props], generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        # Generate proportions using Dirichlet distribution
        dirichlet_proportions = np.random.dirichlet([alpha] * num_clients)
        # Split dataset according to Dirichlet proportions
        datasets = random_split(trainset, [int(len(trainset) * p) for p in dirichlet_proportions], generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)
        class_per_client = num_classes / num_clients
        data_indices_each_client = {client: [] for client in range(num_clients)}
        for c in range(num_classes):
            indices = (torch.tensor(trainset.targets)[..., None] == c).any(-1).nonzero(as_tuple=True)[0]
            client_belong = int(c / class_per_client)
            data_indices_each_client[client_belong].extend(list(indices))
        datasets = []
        for i in range(num_clients):
            datasets.append(Subset(trainset, data_indices_each_client[i]))
    else:
        raise NotImplementedError("The data split is not implemented")

    # Split each partition into train/val and create DataLoader
    trainloaders = []
    valloaders = []
    for ds in datasets:
        len_val = len(ds) // 10  # 10% validation set
        len_train = len(ds) - len_val
        lengths = [len_train, len_val]
        ds_train, ds_val = random_split(ds, lengths, generator=torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)
    return trainloaders, valloaders, testloader

      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
    
    **Réponse** :  La taille des jeux de donnees: 
                                  Entrainement : 1399x32  | Evaluation : 156x32
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : La précision finale du modèle est de 0.6176470588235294.
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Les données sont distribuées parmi les clients en mode IID pour une répartition équitable, en mode Non-IID par classe pour une distribution spécifique à certaines classes par client, et une option prévue pour une distribution Non-IID par nombre basée sur la loi de Dirichlet.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : L'observation des résultats indique une amélioration progressive de la performance du modèle au fil des tours de communication dans un contexte d'apprentissage fédéré. Le processus d'entraînement et d'évaluation répété à travers plusieurs tours montre une augmentation de l'efficacité du modèle sur le jeu de données de test. Cette amélioration est visible à travers les taux de réussite plus élevés lors des phases d'évaluation après chaque tour d'entraînement, ce qui suggère que le modèle devient plus précis dans ses prédictions au fur et à mesure qu'il apprend des données distribuées chez différents clients.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : L'exécution est plus rapide principalement parce que le nombre de tours de communication a été réduit, ce qui diminue le temps passé en échanges de données entre le serveur et les clients. Moins de tours signifie moins d'interruptions pour la synchronisation, permettant une progression plus rapide de l'apprentissage malgré l'augmentation des époques locales effectuées par chaque client.


    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : L'augmentation des époques locales à 2 et la réduction des rounds à 5 montrent que le modèle s'améliore toujours au fil du temps, indiquant que plus d'entraînement local par round peut efficacement améliorer la précision même avec moins de communications globales.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :L'exécution avec 2 rounds et 5 époques locales est probablement plus rapide due à moins de communications entre le serveur et les clients, malgré un entraînement local intensifié sur chaque client. La réduction des rounds diminue le temps global de communication, qui est souvent le plus long dans l'apprentissage fédéré.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Avec seulement 2 rounds de communication et 5 époques locales d'entraînement, le modèle aurait moins d'opportunités d'apprendre à partir des mises à jour globales, ce qui pourrait affecter sa capacité à généraliser correctement sur l'ensemble des données. Cependant, l'entraînement local plus intensif sur chaque client pourrait aider à compenser en partie cette limitation, surtout si les données locales sont suffisamment représentatives. La performance finale du modèle pourrait ne pas être aussi élevée qu'avec plus de rounds, reflétant un compromis entre la rapidité d'exécution et la précision du modèle.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python: Nom de fichier c'est prepare_dataset.py
import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split, Subset
from torchvision.datasets import CIFAR10, CIFAR100
import numpy as np

def load_datasets(num_clients: int, dataset, data_split, alpha=0.5):
    # Download and transform CIFAR-10 and CIFAR-100
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )

    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset specified is not implemented.")

    if data_split == "iid":
        props = [1/num_clients]*num_clients
        datasets = random_split(trainset, [int(len(trainset) * p) for p in props], generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        dirichlet_distribution = np.random.dirichlet([alpha] * num_clients)
        num_samples_per_client = [int(partition * len(trainset)) for partition in dirichlet_distribution]
        datasets = random_split(trainset, num_samples_per_client, generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)
        class_per_client = num_classes / num_clients
        data_indices_each_client = {client: [] for client in range(num_clients)}
        for c in range(num_classes):
            indices = (torch.tensor(trainset.targets) == c).nonzero(as_tuple=True)[0]
            client_belong = int(c / class_per_client)
            data_indices_each_client[client_belong].extend(indices.tolist())
        datasets = [Subset(trainset, indices) for indices in data_indices_each_client.values()]
    else:
        raise NotImplementedError("The data split specified is not implemented.")

    trainloaders = []
    valloaders = []
    for ds in datasets:
        len_val = len(ds) // 10
        len_train = len(ds) - len_val
        ds_train, ds_val = random_split(ds, [len_train, len_val], generator=torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)
    return trainloaders, valloaders, testloader

def get_data_loader(num_clients: int, cid: int, dataset="CIFAR10", data_split="iid", alpha=0.5):
    trainloaders, valloaders, testloader = load_datasets(num_clients, dataset, data_split, alpha)
    return trainloaders[cid], valloaders[cid], testloader

      
   
       ```

      ``` sur client.py on modifie la partie du code suivante:
      parser.add_argument(
    "--dataset",
    type=str,
    default="CIFAR10",
    choices=["CIFAR10", "CIFAR100"],  # Ajoutez CIFAR100 comme choix valide
    help="Dataset: [CIFAR10, CIFAR100]",
)
    ```
