**Nom**: MARTINEZ

**Prénom**: Anais

**Date**:19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le jeu de données CIFAR10 a été utilisé pour l'entraînement et l'évaluation dans cet environnement d'apprentissage fédéré.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :
    La fonction load_datasets gère la répartition des données CIFAR-10 entre les clients en fonction du paramètre data_split :

    IID ("iid") : La répartition des données est effectuée de manière identiquement distribuée parmi tous les clients (num_clients). Chaque client reçoit une partition égale du jeu de données d'entraînement.

    Non-IID par nombre ("non_iid_number") : Cette option est prévue mais n'a pas été implémentée. Elle devrait permettre une répartition des données non identique, potentiellement basée sur le nombre d'images.

    Non-IID par classe ("non_iid_class") : Les données sont réparties de sorte que chaque client se voit attribuer des données appartenant à un sous-ensemble spécifique des classes disponibles. Cela est réalisé en divisant le nombre total de classes par le nombre de clients et en attribuant les classes de manière égale entre eux.

    Pour la répartition IID, chaque client reçoit approximativement la même quantité de données. Le jeu de données d'entraînement de CIFAR-10 est divisé également entre les clients. Étant donné que CIFAR-10 comprend 50 000 images d'entraînement, si nous avons par exemple 5 clients, chaque client recevrait 10 000 images pour l'entraînement. La division pour la validation est déterminée en séparant 10% de chaque partition de données d'entraînement pour la validation (ligne 46), ce qui signifie que chaque client utiliserait 9 000 images pour l'entraînement et 1 000 images pour la validation.

    Pour la répartition Non-IID par classe, le nombre d'images par client dépendrait du nombre de classes attribuées à chaque client et de la distribution des classes dans le jeu de données CIFAR-10. Comme la répartition est basée sur les classes, le nombre total d'images variera en fonction des classes spécifiques attribuées à chaque client. La séparation en ensembles d'entraînement et de validation suit la même logique de 10% pour la validation mentionnée ci-dessus.

    Le chargeur de test (testloader) est commun et utilise le jeu de données de test de CIFAR-10, qui contient 10 000 images, permettant une évaluation uniforme des modèles formés par les clients.

    Ces informations sont directement trouvées et interprétées à partir de la fonction load_datasets dans prepare_dataset.py, qui définit explicitement la logique de répartition des données et la préparation des chargeurs de données (DataLoader) pour l'entraînement et la validation.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Un modèle de réseau de neurones convolutif (Convolutional Neural Network, CNN) a été utilisé pour le test. Ce modèle est adapté pour des tâches de classification d'images, comme celles impliquant le jeu de données CIFAR10.

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :Le modèle comporte plusieurs couches:
    Deux couches de convolution (nn.Conv2d) avec un pooling (nn.MaxPool2d) après chaque convolution.
    Trois couches entièrement connectées (nn.Linear).
    La structure détaillée est :
    Convolution avec 3 canaux en entrée, 6 en sortie, kernel de taille 5.
    Max pooling avec un kernel de taille 2 et un stride de 2.
    Convolution avec 6 canaux en entrée, 16 en sortie, kernel de taille 5.
    Trois couches linéaires transformant les caractéristiques de la forme 16 * 5 * 5 à 120, puis à 84, et finalement à 10 classes de sortie.

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :
    L'algorithme d'optimisation utilisé est SGD (Descente de Gradient Stochastique) avec un taux d'apprentissage de 0.01 et un momentum de 0.9. Ces détails sont trouvés dans la fonction train du script client.py.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Chaque client utilise le nombre d'époques spécifié par le paramètre --local_epochs pour entraîner son modèle local. Par défaut, ce nombre est fixé à 1 mais peut être ajusté via la ligne de commande lors du lancement du script client. Ici l'argumant n'était pas préciser alors les epochs sont a 1 pour les 2 clients.

    Cette information se trouve dans le script client.py, principalement autour des lignes 97-101 pour la définition du paramètre --local_epochs, et la ligne 130 où ce paramètre est utilisé pour définir le nombre d'époques lors de l'appel à la fonction train.

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
    Le nombre total de tours de communication est défini dans le script server.py par le paramètre --rounds. Ce paramètre est configuré pour accepter un argument entier et possède une valeur par défaut de 10. Cela signifie que, sauf spécification contraire lors du lancement du serveur, il y aura un total de 10 tours de communication entre le serveur et les clients pendant le processus d'entraînement fédéré.

    Cette information se trouve au début du script server.py, où le parseur d'arguments est défini et configuré pour reconnaître l'option --rounds (ligne 7-12)

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** :
    L'évaluation dans ce cadre d'apprentissage fédéré est réalisée par les clients. Chaque client effectue une évaluation locale de son modèle sur son propre jeu de données de validation après avoir mis à jour le modèle avec les paramètres globaux envoyés par le serveur.

    Dans le script client.py, la méthode evaluate de la classe FlowerClient est responsable de cette évaluation. Elle reçoit les paramètres du modèle global du serveur, met à jour le modèle local avec ces paramètres, puis évalue ce modèle sur le jeu de données de validation local. Le résultat de l'évaluation, incluant la perte et l'exactitude (accuracy), est ensuite renvoyé au serveur. Cela est visible dans les lignes 93 à 98 du script client.py, où la fonction test est appelée avec le valloader (le chargeur de données de validation) pour effectuer l'évaluation.

    Les tailles des jeux de données pour l'évaluation dépendent de la répartition initiale des données entre les clients. Comme mentionné précédemment, la répartition peut être IID ou non-IID, et une fraction des données de chaque client est réservée pour la validation. Dans le script prepare_dataset.py, une règle générale est d'allouer 10 % du jeu de données de chaque client pour la validation (ligne 288-291). Ainsi, si un client se voit attribuer 5 000 images pour l'entraînement (dans un scénario hypothétique IID avec 50 000 images réparties entre 10 clients), 500 de ces images seraient utilisées pour la validation.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Cette logique est implantée dans le script server.py, spécifiquement dans la fonction weighted_average qui est appelée par la stratégie d'agrégation des métriques d'évaluation (evaluate_metrics_aggregation_fn) configurée dans la stratégie FedAvg du serveur Flower (ligne 23-32).

3. Quelle est la précision finale du modèle ?

    **Réponse** : D'après le log :
    0.3928 0.4844 0.5376 0.5632 0.5998 0.5886 0.6018 0.6134 0.6146 0.625.
    Cela indique que, suite à l'agrégation des mises à jour des clients et à l'amélioration successive du modèle global à travers les tours, la précision sur le jeu de données d'évaluation a atteint 62.5 % à la fin du processus d'entraînement fédéré (après le 10ème et dernier tour ).

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** : Dans la section du code dans prepare_dataset.py où data_split == "non_iid_number" est mentionné mais pas encore implémenté :
    ```python
    elif data_split == "non_iid_number":
        alpha = 0.8 
        proportions = np.random.dirichlet([alpha] * num_clients)
        total_size = len(trainset)
        indices = np.arange(total_size)
        np.random.shuffle(indices)  # Mélanger les indices pour garantir la randomisation
        datasets = []
        start = 0
        for prop in proportions:
            end = start + int(prop * total_size)
            datasets.append(Subset(trainset, indices[start:end]))
            start = end
        for i, dataset in enumerate(datasets):
            print(f"dataset {i}: {len(dataset)} images")
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : 
      print du 1er Client : 
      dataset 0: 2584 images
      dataset 1: 47415 images

      print du 2 eme Client :
      dataset 0: 19723 images
      dataset 1: 30276 images

   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : Dáprès le log:
      0.39452054794520547 0.493455098934551 0.5190258751902588 0.5546423135464231 0.5613394216133942 0.5698630136986301 0.5753424657534246 0.5905631659056316 0.6097412480974125 0.6063926940639269
      Le modèle a atteint un précision finale de 60.64%
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --round 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :
        Lorsque le code est exécuté avec l'option --data_split non_iid_class, la distribution des données parmi les clients se fait de manière à ce que chaque client reçoive des sous-ensembles de données contenant des classes spécifiques, favorisant ainsi une distribution non identiquement distribuée (non-IID) des données.

        Cela signifie que chaque client travaillera sur un ensemble de données qui pourrait être concentré sur un ou plusieurs types spécifiques de classes parmi celles disponibles dans le jeu de données CIFAR-10, ce qui entraîne une variété dans l'apprentissage local en fonction des classes de données attribuées à chaque client. Cette méthode vise à simuler des scénarios d'apprentissage fédéré où les données ne sont pas uniformément réparties parmi les clients en termes de types de données ou de classes, reflétant des distributions de données plus réalistes rencontrées dans des applications du monde réel.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : Dáprès le log:
      0.2544 0.3632 0.4504 0.437 0.4798 0.487 0.4808 0.4726 0.4422 0.5032 

      L'observation des résultats montre que la précision finale du modèle avec une distribution non-IID par classe (data_split non_iid_class) est d'environ 50.32%, ce qui est inférieur aux résultats précédents où nous obtenions environ 60% de précision avec une distribution différente. Cette différence peut s'expliquer par la nature non-IID des données, où chaque client se concentre sur un ensemble plus restreint de classes, ce qui pourrait limiter la capacité du modèle global à généraliser sur l'ensemble du jeu de données. Cela souligne l'impact significatif que la distribution des données peut avoir sur la performance du modèle dans les scénarios d'apprentissage fédéré.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :

        Le temps d'execution est plus ou moins le meme car réduire le nombre de rounds à 5 accélére l'exécution en termes de temps réel parce que moins de tours de communication globaux sont nécessaires. Cependant, augmenter le nombre d'époques locales a 2 augmente le temps de calcul sur chaque client, ce qui doit plus ou moins compenser la réduction du temps due a round=5.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
          Avec une précision finale de 49.52% dans le scénario avec 5 rounds et 2 époques locales pour une distribution non-IID par classe, la performance est légèrement inférieure à celle observée dans l'expérience précédente où la précision était d'environ 50.32%. Cela indique qu'avec moins de rounds de communication et plus d'époques locales, le modèle n'a pas nécessairement montré d'amélioration de la précision, soulignant l'importance de l'équilibre entre le nombre de rounds de communication et le nombre d'époques locales pour optimiser la performance du modèle dans des conditions non-IID.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
        Réduire le nombre de round à 2 et augmenter réduit considérablement le temps de communication entre le serveur et les clients avec moins de tours. Cependant, augmenter le nombre d'époques locales augmente le temps de traitement sur chaque client ce qui compense encore une fois plus ou moins le temps gagné lors de l'execution avec round à 2

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

       **Réponse** :
        Avec seulement 2 rounds et 5 époques locales, la précision finale du modèle est d'environ 39.78%, ce qui est inférieur aux expériences précédentes. Cette baisse de précision suggère que, bien que l'augmentation des époques locales puisse approfondir l'apprentissage sur les données locales de chaque client, le nombre très limité de rounds de communication entre le serveur et les clients réduit l'opportunité d'améliorer le modèle global à travers l'agrégation des mises à jour. Cela souligne l'importance d'un nombre suffisant de rounds de communication pour permettre une convergence et une amélioration efficaces du modèle global dans l'apprentissage fédéré.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** : prepare_dataset.py

       ```python
       from torchvision.datasets import CIFAR10, CIFAR100
       
       #dans la fonction load_datasets du fichier prepare_dataset.py

       elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)

        #dans la fonction __init__ de la classe Net du fichier client.py

        if dataset == "CIFAR10":
            out_features = 10
        elif dataset == "CIFAR100":
            out_features = 100
        else:
            raise ValueError("Unsupported dataset")
        self.fc3 = nn.Linear(84, out_features)
       ```