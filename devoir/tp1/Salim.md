**Nom**: Chalhaj 
**Prénom**: Salim

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Le jeu de données testé est CIFAR-10** :
   `trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
   testset = CIFAR10("./dataset", train=False, download=True, transform=transform)`


2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Les données sont réparties entre les clients de manière indépendante et identiquement distribuée (iid)** :
    La répartition iid est réalisée en divisant le dataset d'entraînement en num_clients partitions de taille égale, comme le montre le code suivant :
    `if data_split == "iid":`
   ` props = [1/num_clients]*num_clients
    datasets = random_split(trainset, props, torch.Generator().manual_seed(42))`
    Quant au nombre d'images utilisées pour l'entraînement et la validation par chaque client, cela dépend de la taille de la partition attribuée à chaque client après la division du dataset d'entraînement. En général, chaque client reçoit 1/num_clients du dataset d'entraînement. Ensuite, chaque partition est divisée en un ensemble d'entraînement et un ensemble de validation, avec une division typique de 90% pour l'entraînement et 10% pour la validation :
   ` len_val = len(ds) // 10  # 10 % validation set
    len_train = len(ds) - len_val`
Cependant, le nombre exact d'images dépend de la taille totale du dataset CIFAR-10 (50 000 images d'entraînement) et du nombre de clients (num_clients). Par exemple, si le dataset est divisé entre 10 clients de manière iid, chaque client recevrait approximativement 5 000 images pour l'entraînement, dont 10 % (soit environ 500 images) seraient utilisées pour la validation et le reste pour l'entraînement.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

Le modèle testé est un réseau de neurones convolutionnel (CNN). Il est conçu pour le traitement d'images, ce qui est cohérent avec l'utilisation du jeu de données CIFAR-10.


2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?

Réponse : Le modèle comporte plusieurs couches, y compris des couches convolutionnelles, de pooling, et des couches entièrement connectées (linear).

 le modèle comporte 2 couches convolutionnelles, 1 couche de pooling (utilisée deux fois), et 3 couches entièrement connectées  


### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 

 Pour l'entraînement du modèle local, chaque client a utilisé l'algorithme de descente de gradient stochastique (SGD) avec un taux d'apprentissage de 0.01 et un momentum de 0.9. 

Où pouvons-nous trouver cette information dans le code ?

`optimizer = torch.optim.SGD(net.parameters(), lr=0.01, momentum=0.9)
`

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ?
Où pouvons-nous trouver cette information dans le code ? 
Le nombre d'époques de calcul local effectuées pour chaque client est spécifié par le paramètre --local_epochs dans la ligne de commande lors du démarrage du client. La valeur par défaut est de 1 époque, comme indiqué dans la définition des arguments de la ligne de commande dans la première classe (client.py) :
`parser.add_argument(
    "--local_epochs",
    type=int,
    default=1,
    help="époques",
)
` 
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

 Le nombre total de tours de communication est spécifié par le paramètre --round dans la ligne de commande lors du démarrage du serveur, dans la troisième classe (server.py). La valeur par défaut est de 10 tours, 
`
parser.add_argument(
    "--round",
    type=int,
    default=10,
    help="Number of rounds of federated learning",
)
`
### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

L'évaluation est réalisée par les clients, qui testent le modèle sur leur propre jeu de données de validation local. Après l'entraînement local, chaque client évalue la performance du modèle à l'aide de sa fonction evaluate
la taille du jeu de données de validation de chaque client serait environ 10% de sa partition de données.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

L'enregistrement des résultats de l'évaluation dans le fichier "log.txt" est effectué dans la fonction weighted_average de la troisième classe (server.py)

`try:
    with open('log.txt', 'a') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy)+" ")
except FileNotFoundError:
    with open('log.txt', 'w') as f:
        if round == 1:
            f.write("\n-------------------------------------\n")
        f.write(str(accuracy)+" ")
`
3. Quelle est la précision finale du modèle ?

La precision : 0.6264 

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
````import numpy as np  # Assurez-vous d'importer numpy

def load_datasets(num_clients: int, dataset, data_split, alpha=0.5): 
    # Download and transform CIFAR-10 (train and test)
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset is not implemented")

    if data_split == "iid":
        props = [1/num_clients]*num_clients
        datasets = random_split(trainset, [int(prop * len(trainset)) for prop in props], generator=torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        # Utilisez la loi de Dirichlet pour déterminer les proportions des données pour chaque client
        proportions = np.random.dirichlet([alpha] * num_clients)
        # Assurez-vous que la somme des proportions est égale à 1
        proportions = proportions / proportions.sum()
        split_sizes = [int(prop * len(trainset)) for prop in proportions]
        datasets = random_split(trainset, split_sizes, generator=torch.Generator().manual_seed(42))
    else:
        raise NotImplementedError("The data split is not implemented")

    # Split each partition into train/val and create DataLoader
    trainloaders = []
    valloaders = []
    for ds in datasets:
        len_val = len(ds) // 10  # 10 % validation set
        len_train = len(ds) - len_val
        lengths = [len_train, len_val]
        ds_train, ds_val = random_split(ds, lengths, torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)
    return trainloaders, valloaders, testloader
      
   
    ```
````
2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       

Avec un paramètre alpha=0.8 pour la distribution de Dirichlet répartie entre 2 clients, les tailles des jeux de données pour chaque client seraient approximativement les suivantes :

Client 1 : 40 372 images
Client 2 : 9 628 images

   2. Quelle est la précision finale du modèle ?
   
    la precision final est de 0.6338    
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
les données sont distribuées parmi les clients selon le paramètre --data_split non_iid_class. Cela signifie que la répartition des données entre les clients est basée sur les classes du jeu de données. Chaque client reçoit des données de manière à ce que la distribution des classes ne soit pas identique entre eux, ce qui rend la distribution non-IID (non identiquement distribuée) par classe.
  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
 Cela a  affecté la généralisation du modèle et sa précision globale par rapport à une distribution IID des données.  
3. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
Réduire le nombre de rounds à 5 et augmenter le nombre d'époques locales à 2 pourrait potentiellement accélérer le processus global d'apprentissage fédéré. Moins de rounds signifie moins de cycles de communication entre les clients et le serveur, ce qui peut réduire le temps total d'exécution. Cependant, augmenter les époques locales à 2 signifie que chaque client effectuera plus de travail localement avant de synchroniser avec le serveur   

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

 Avec plus d'époques locales, chaque client a l'opportunité d'apprendre plus efficacement à partir de ses données locales, ce qui peut améliorer la performance du modèle local avant la mise à jour globale
5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
     Réduire encore le nombre de rounds à 2 tout en augmentant les époques locales à 5 réduit significativement le nombre de communications nécessaires entre les clients et le serveur, ce qui devrait accélérer le processus global. Cependant, avec autant d'époques locales, chaque client passe plus de temps à apprendre de ses données locales, ce qui pourrait ou non compenser en termes de durée totale, en fonction de la balance entre le coût de calcul local et le coût de communication.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
    Avec un nombre accru d'époques locales et moins de rounds, les clients ont encore plus l'opportunité de bien s'adapter à leurs données locales. Cela pourrait améliorer la performance des modèles locaux mais également augmenter le risque de surapprentissage sur les données non-IID, ce qui peut nuire à la généralisation du modèle global. La performance globale du modèle peut donc varier, potentiellement améliorant la précision

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

  from torchvision.datasets import CIFAR10, CIFAR100  # Importez CIFAR100

def load_datasets(num_clients: int, dataset, data_split, alpha=0.8):  # Gardez l'argument alpha pour la loi de Dirichlet
    # Transformations communes pour CIFAR10 et CIFAR100
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )

    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError("The dataset is not implemented")

    # La suite du code reste inchangée, gérant les différentes stratégies de division des données...
``