**LE CONTEL**:

**Loïc**:

**19/02/2024**:

# TP1 : Introduction de l'apprentissage fédéré

## Installation par conda

* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower

  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez

* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :

  ```bash
  > cd TP1_flower_pytorch
  ```

* Dans Terminal 1:

  ```bash
  > python server.py
  ```

* Dans Terminal 2:

  ```bash
  > python client.py --node_id 0
  ```

* Dans Terminal 3:

  ```bash
  > python client.py --node_id 1
  ```

## Questions

### Jeux de données

1. Quel jeu de données ont été testé ?

   **Réponse** : Le jeu de données testé est CIFAR10. (vu dans prepare_dataset.py)

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ?
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
  **Réponse** : Les données sont réparties entre les clients de manière non-iid. Les données sont réparties par classe. (vu dans prepare_dataset.py) Chaque client utilise 5000 images pour l'entraînement et 1000 images pour la validation.

### Modèle d'entraînement

1. Quel type de modèle a été testé ?

   **Réponse** : Le modèle testé est un réseau de neurones convolutif (CNN).

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle comporte 3 couches de convolution, 2 couches de pooling, 2 couches de normalisation, 2 couches de dropout et 2 couches entièrement connectées.

### Le processus d'entraînement

1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ?
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : L'algorithme appliqué pour l'entraînement du modèle local est la descente de gradient stochastique (SGD). (vu dans client.py)

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ?
Où pouvons-nous trouver cette information dans le code ?
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*.

    **Réponse** : Chaque client effectue 1 époque de calcul local. (vu dans client.py)

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Il y a 10 tours de communication au total. (vu dans server.py)

### Evaluation

1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** : L'évaluation est réalisée par le serveur. Les tailles des jeux de données pour l'évaluation sont 22500 pour l'entraînement et 2500 pour la validation

2. Les résultats de l'évaluation sont entregistrés dans le fichier "log.txt". Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Les résultats de l'évaluation sont enregistrés dans le fichier "log.txt" dans le dossier "logs". (vu dans server.py)

3. Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale du modèle est 0.6232.

## Exercices

1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par [le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet).

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n)`(*alpha* est
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :

    ```python
    def load_datasets(num_clients: int, dataset, data_split, alpha=0.5):
      [...]
      elif data_split == "non_iid_number":
        alpha = 0.8
        proportions = torch.tensor(np.random.dirichlet([alpha] * num_clients))

        # Calcul des indices de début pour chaque partition
        total_size = len(trainset)
        indices = torch.randperm(total_size)
        partition_sizes = [int(proportion * total_size) for proportion in proportions]
        # Ajustement pour s'assurer que la somme des partitions correspond à la taille totale
        partition_sizes[-1] = total_size - sum(partition_sizes[:-1])

        # Création des partitions
        datasets = []
        start_index = 0
        for size in partition_sizes:
            end_index = start_index + size
            datasets.append(Subset(trainset, indices[start_index:end_index]))
            start_index = end_index
      [...]
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?

      **Réponse** : 
      
      **Client 1**: 6914 train samples and 768 validation samples

      **Client 2**: 38087 train samples and 4231 validation samples

   2. Quelle est la précision finale du modèle ?

      **Réponse** : Précision finale **0.6090086384204031**

3. Lancer votre code suivant:

* Dans Terminal 1:

  ```bash
  > python server.py --round 10
  ```

* Dans Terminal 2:

  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```

* Dans Terminal 3:

  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```

  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?

      **Réponse** : Les données sont distribuées parmi les clients de manière non-iid par classe.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

     **Réponse** : La précision finale diminue. (**0.4918**)
  
  3. Restez le code avec les même commandes sauf round=5, local_epochs=2.
        1. Est-ce que l'execution est plus rapide ? Pourquoi ?
        
            **Réponse** : Oui, l'exécution est plus rapide car il y a moins de tours de communication.
        
        2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       
            **Réponse** : La précision finale est: **0.4626**. Elle est plus basse que la précédente.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapide ? Pourquoi ?
       
       **Réponse** : Oui, l'exécution est plus rapide car il y a moins de tours de communication.

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       
       **Réponse** : La précision finale est: **0.3914**. Elle est plus basse que la précédente. C'est normal car il y a moins de tours de communication.

6. Compléter le code avec l'autre jeu de données "CIFAR100" (fournie par torchvision)

    **Réponse** :
**CLIENT**
```python
class Net(nn.Module):
    """Model"""

    def __init__(self) -> None:
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 100)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return self.fc3(x)


    def train(net, trainloader, epochs):
        """Train the model on the training set."""
        criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(net.parameters(), lr=0.01, momentum=0.9)
        for _ in range(epochs):
            for images, labels in tqdm(trainloader, "Training"):
                optimizer.zero_grad()
                criterion(net(images.to(DEVICE)), labels.to(DEVICE)).backward()
                optimizer.step()


    def test(net, valloader):
        """Validate the model on the validation set."""
        criterion = torch.nn.CrossEntropyLoss()
        correct, loss = 0, 0.0
        with torch.no_grad():
            for images, labels in tqdm(valloader, "Testing"):
                outputs = net(images.to(DEVICE))
                labels = labels.to(DEVICE)
                loss += criterion(outputs, labels).item()
                correct += (torch.max(outputs.data, 1)[1] == labels).sum().item()
        accuracy = correct / len(valloader.dataset)
        return loss, accuracy


# #############################################################################
# 2. Federation of the pipeline with Flower
# #############################################################################

# Get node id
parser = argparse.ArgumentParser(description="Flower")
parser.add_argument(
    "--node_id",
    choices=[0, 1, 2],
    required=True,
    type=int,
    help="Partition of the dataset divided into 3 iid partitions created artificially.",
)
parser.add_argument(
    "--n",
    type=int,
    default=2,
    help="The number of clients in total",
)
parser.add_argument(
    "--dataset",
    type=str,
    default="CIFAR100",
    help="Dataset: [CIFAR100]",
)
parser.add_argument(
    "--data_split",
    type=str,
    default="iid",
    help="iid",
)
parser.add_argument(
    "--local_epochs",
    type=int,
    default=1,
    help="époques",
)
cid = parser.parse_args().node_id
n = parser.parse_args().n
data_split = parser.parse_args().data_split
dataset = parser.parse_args().dataset
local_epochs = parser.parse_args().local_epochs

# Define Flower client
class FlowerClient(fl.client.NumPyClient):
    def __init__(self, cid):
        self.cid = cid
        self.net = Net().to(DEVICE)
        self.trainloader, self.valloader, _ = prepare_dataset.get_data_loader(n, cid, data_split=data_split, dataset=dataset)

    def get_parameters(self, config):
        return [val.cpu().numpy() for _, val in self.net.state_dict().items()]

    def set_parameters(self, parameters):
        params_dict = zip(self.net.state_dict().keys(), parameters)
        state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
        self.net.load_state_dict(state_dict, strict=True)

    def fit(self, parameters, config):
        # Read values from config
        server_round = config["server_round"]
        # Use values provided by the config
        print(f"[Client {self.cid}, round {server_round}] fit")
        self.set_parameters(parameters)
        train(self.net, self.trainloader, epochs=local_epochs)
        return self.get_parameters(config={}), len(self.trainloader.dataset), {}

    def evaluate(self, parameters, config):
        server_round = config["server_round"]
        print(f"[Client {self.cid}, round {server_round}] evaluate, config: {config}")
        self.set_parameters(parameters)
        loss, accuracy = test(self.net, self.valloader)
        return loss, len(self.valloader.dataset), {"accuracy": accuracy, "round": server_round}


      # Start Flower client
      fl.client.start_client(
          server_address="127.0.0.1:8080",
          client=FlowerClient(cid).to_client(),
      )
      
      from torchvision.datasets import CIFAR10, CIFAR100

      if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
      elif dataset == "CIFAR100":
          trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
          testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
      else:
          raise NotImplementedError(
            "The dataset is not implemented"
          )
```
