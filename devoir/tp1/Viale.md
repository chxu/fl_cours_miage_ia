**Nom**: VIALE

**Prénom**: Alexandre

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré

## Installation par conda :

- Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
- [Installation de pytorch](https://pytorch.org/)
- Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:

- Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
- Dans Terminal 1:
  ```bash
  > python server.py
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
- Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:

### Jeux de données

1.  Quels jeux de données ont été testé ?

    **Réponse** : Le CIFAR 10

2.  Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ?
    Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
    **Réponse** : Si data_split est défini sur "iid", le jeu de données est divisé de manière identique et indépendamment distribuée (IID) entre les clients. Cela signifie que chaque client reçoit un sous-ensemble aléatoire du jeu de données complet. Cette division est réalisée par la fonction random_split.
    Cette information se trouve ici :
    `` python
    if data_split == "iid":
    # Split training set into `num_clients` partitions to simulate different local datasets
    props = [1/num_clients]\*num_clients
    ``
    S'il y a 2 clients :
    Chaque client utilisera 22 500 images pour l'entraînement.
    Chaque client disposera de 2 500 images pour la validation.

### Modèle d'entraînement

1. Quel type de modèle a été testé ?

   **Réponse** : C'est un CNN

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?

   **Réponse** : Le modèle comporte 8 couches principales : 2 couches convolutives, 2 couches de pooling max, et 4 couches linéaires (entièrement connectées). Sa structure alterne couches convolutives et pooling, suivies de couches linéaires pour la classification.

### Le processus d'entraînement

1.  Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ?
    Où pouvons-nous trouver cette information dans le code ?

        **Réponse** : Pour chaque client, l'algorithme d'entraînement du modèle local utilise la descente de gradient avec un taux d'apprentissage de 0.01 et un momentum de 0.9.
        Cette information se trouve dans la fonction `train` du fichier client.py, où `torch.optim.SGD` est instancié avec ces paramètres pour l'optimiseur.

2.  Pour chaque client, combien d'époques de calcul local ont été effectuées ?
    Où pouvons-nous trouver cette information dans le code ?  
    Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une _époque_.

        **Réponse** : Pour chaque client, le nombre d'époques de calcul local effectuées est déterminé par le paramètre `--local_epochs`, qui est passé lors de l'exécution du script `client.py`. Par défaut, ce paramètre est fixé à 1, signifiant qu'une époque de calcul local est effectuée. Cette information peut être trouvée dans la section où `parser.add_argument` pour `--local_epochs` est défini dans `client.py`, ainsi que dans la fonction `fit` de la classe FlowerClient où train est appelé avec `epochs=local_epochs`.

3.  Combien de tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est déterminé par le paramètre `--rounds`, qui est spécifié lors de l'exécution du script `server.py`. Par défaut, ce paramètre est fixé à 10, ce qui signifie qu'il y aura 10 tours de communication entre le serveur et les clients. Cette information peut être trouvée dans la section où `parser.add_argument` pour `--rounds` est défini dans `server.py`, et également là où `fl.server.start_server` est appelé avec `config=fl.server.ServerConfig(num_rounds=rounds)`, ce qui configure le serveur pour effectuer le nombre spécifié de tours de communication.

### Evaluation

1.  Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ?
    Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation est réalisée par les clients sur leurs propres jeux de données de validation, comme défini dans la méthode `evaluate` de la classe `FlowerClient` dans `client.py`. La taille des jeux de données pour l'évaluation varie en fonction de la partition initiale du dataset d'entraînement, avec 10 % réservés à la validation, dont la quantité exacte dépend du nombre de clients et de la stratégie de répartition des données.

2.  Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
    Où nous pouvons trouver cette information dans le code ?

        **Réponse** : Les résultats de l'évaluation sont enregistrés dans `log.txt` par la fonction `weighted_average` dans `server.py`, qui écrit la précision globale dans le fichier après chaque tour d'évaluation.

3.  Quelle est la précision finale du modèle ?

    **Réponse** : Voici les résultats de la précision finale du modèle pour chaque tour d'évaluation (dans le fichier `log.txt`) :
    `0.416 0.5042 0.5406 0.5664 0.5766 0.5948 0.6122 0.61 0.6148 0.627`

## Exercices:

1.  Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par
    [le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet).

        Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est

    un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit,
    plus les différences dans le nombre d'images entre les clients sont grandes; _n_ est le nombre de client total)

        **Réponse** :
        ```python
        # Utilisation de la loi de Dirichlet pour la distribution non-iid basée sur le nombre
        proportions = np.random.dirichlet([alpha] * num_clients)
        # Calcul des tailles des partitions pour chaque client basé sur les proportions
        partitions_sizes = [int(proportion * len(trainset)) for proportion in proportions]
        # Ajustement pour s'assurer que la somme des tailles des partitions égale la taille totale du dataset
        partitions_sizes[-1] = len(trainset) - sum(partitions_sizes[:-1])
        datasets = random_split(trainset, partitions_sizes, torch.Generator().manual_seed(42))
        ```

2.  Retester votre code avec _alpha=0.8_

    1. Quelles sont les tailles des jeux de données pour chaque client ?

       **Réponse** : Dans cet exemple, les proportions générées par la distribution de Dirichlet étaient d'environ 0.429 et 0.571. Multipliant ces proportions par 50 000 donne les tailles des partitions : environ 21 453 images pour le premier client et 28 547 images pour le second.

    2. Quelle est la précision finale du modèle ?

       **Réponse** : `0.4083191850594228 0.4904499151103565 0.5350169779286927 0.5721561969439728 0.5874363327674024 0.6071731748726655 0.6122665534804754 0.6160865874363328 0.6300933786078099 0.6177843803056027`

3.  Lancer votre code suiant:

- Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```
- Dans Terminal 3:

  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```

  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?  
     **Réponse** : Avec --data_split non_iid_class, les données devraient être réparties de manière à ce que chaque client reçoive des images de certaines classes uniquement, introduisant une hétérogénéité dans la distribution des données. Cette fonctionnalité nécessite une implémentation manuelle dans prepare_dataset.py.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
     **Réponse** : Les résultats sont moins bons. La précision finale du modèle est plus faible que dans les tests précédents, ce qui suggère que la distribution non-iid basée sur les classes a un impact négatif sur les performances du modèle.
     `0.2626 0.3834 0.4342 0.4582 0.4482 0.4956 0.5048 0.5164 0.5162 0.536`

4. Restez le code avec les même commandes sauf round=5, local_epochs=2.

   1. Est-ce que l'execution est plus rapide ? Pourquoi ?
      **Réponse** : Non l'exécution n'est pas plus rapide, nous avons réduit le nombre de rounds mais augmenté le nombre d'époques de calcul local, ce qui signifie que le temps d'exécution total est similaire.
      J'ai mesuré 1m24s pour 10 rounds avec 1 époque, et 1m23s pour 5 rounds avec 2 époques.
   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      **Réponse** : `0.2986 0.421 0.4556 0.485 0.4982` Au bout de 5 epochs, on converge plus rapidement vers un modèle plus performant, ce qui est normal puisque chaque client a plus d'opportunités pour ajuster les poids du modèle.
      Cependant, précédemment, au bout de 10 epochs, on avait `0.2626 0.3834 0.4342 0.4582 0.4482 0.4956 0.5048 0.5164 0.5162 0.536` ce qui donne de meilleurs résultats avec l'expérience précédente.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.

   1. Est-ce que l'execution est plus rapid ? Pourquoi ?
      **Réponse** : Non le temps d'exécution est identique, comme vu précédemment, le temps d'exécution est déterminé par le nombre d'époques de calcul local, et non par le nombre de rounds., on arrive donc à 1m23s pour 5 rounds avec 2 époques, et 1m23s pour 2 rounds avec 5 époques.
   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      **Réponse** : `0.3104 0.4272` Au bout de 2 epochs, on converge plus rapidement vers un modèle plus performant, ce qui est normal puisque chaque client a plus d'opportunités pour ajuster les poids du modèle.
      Cependant, précédemment, au bout de 5 epochs, on avait `0.2986 0.421 0.4556 0.485 0.4982` ce qui donne de meilleurs résultats avec l'expérience précédente.

6. Compléter le code avec l'autre jeu de données "CIFAR100" (fournie par torchvision)

   **Réponse** :

   ```python
    # Fichier prepare_dataset.py
    # Voici les modifications que j'ai apportées pour charger le jeu de données CIFAR100 :
    def load_datasets(num_clients: int, dataset, data_split):
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize(
            (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR100":
        trainset = CIFAR100(root='./dataset', train=True,
                            download=True, transform=transform)
        testset = CIFAR100(root='./dataset', train=False,
                           download=True, transform=transform)
    else:
        raise NotImplementedError(f"The dataset {dataset} is not implemented")

    # Fichier client.py
    ## Voici les modifications que j'ai apportées pour charger le jeu de données CIFAR100 :
    parser.add_argument(
    "--dataset",
    type=str,
    default="CIFAR100",
    help="Dataset: [CIFAR100]"
    )
    def get_data_loader(num_clients: int, cid: int, dataset="CIFAR100", data_split="iid"):
    trainloaders, valloaders, testloader = load_datasets(
        num_clients, dataset, data_split)
    return trainloaders[cid], valloaders[cid], testloader
    ## à la ligne 29, j'ai passé le nn.linear(84, 100) à la place de nn.liear(84, 10)
    self.fc3 = nn.Linear(84, 100)
   ```
