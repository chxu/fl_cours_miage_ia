**Nom**:

**Prénom**:

**Date**:

#TP1 : Introduction de l'apprentissage fédéré

## Installation par conda :

- Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
- [Installation de pytorch](https://pytorch.org/)
- Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:

- Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
- Dans Terminal 1:
  ```bash
  > python server.py
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
- Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:

### Jeux de données

1.  Qeul jeu de données ont été testé ?

    **Réponse** : Le jeu de données utilisé dans cette configuration d'apprentissage fédéré est CIFAR-10.

2.  Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ?
    Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?

    **Réponse** : Les données sont réparties entre les clients de manière indépendante et identiquement distribuée (IID), comme le suggère l'argument --data_split=iid utilisé dans le script client.py.

Chaque client utilise 22500 images pour l'entraînement et 2500 images pour la validation.

### Modèle d'entraînement

1.  Quel type de modèle a été testé ?

    **Réponse** : Le modèle testé dans ce scénario d'apprentissage fédéré est un modèle de réseau de neurones convolutionnel (CNN)

2.  Combien de couches comporte ce modèle et veuillez détailler sa structure ?

    **Réponse** : Ce modèle comporte un total de sept couches, réparties comme suit :

    1. Couche de convolution : `self.conv1`
    2. Couche de pooling : `self.pool` (utilisée deux fois)
    3. Couche de convolution : `self.conv2`
    4. Couche entièrement connectée (fully connected) : `self.fc1`
    5. Couche entièrement connectée (fully connected) : `self.fc2`
    6. Couche entièrement connectée (fully connected) : `self.fc3`
    7. Couche de fonction d'activation : utilisée à chaque fois après l'application des couches linéaires (fully connected) et convolutives.

### Le processus d'entraînement

1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ?
   Où pouvons-nous trouver cette information dans le code ?

   **Réponse** : Pour l'entraînement du modèle local sur chaque client dans le scénario d'apprentissage fédéré, l'algorithme d'optimisation utilisé est Stochastic Gradient Descent (SGD) avec un momentum. Cette information est spécifiée dans le script client.py au sein de la fonction train, qui est responsable de l'entraînement du modèle sur les données locales du client.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ?
   Où pouvons-nous trouver cette information dans le code ?  
   Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une _époque_.

   **Réponse** : Le nombre d'époques local est par défaut, 1. On retrouve cette information dans le fichier `client.py` au niveau de la variable local_epochs

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

   **Réponse** : Le nombre de rounds est 10 par défaut. On retrouve cette information dans le fichier `server.py` au niveau de la variable rounds.

### Evaluation

1.  Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ?
    Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** : L'évaluation du modèle est faite en local par le client, on peut verifier cela dans la fonction test du fichier `client.py`. Cependant, la mise a jours des poids est faites depuis le serveur au niveau de la fonction weighted_average du fichier `server.py`.

2.  Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
     Où nous pouvons trouver cette information dans le code ?

    **Réponse** : On peut retrouver cela dans la fonction weighted_average du fichier `server.py`. Plus précisément :

    ````python
    try:
    with open('log.txt', 'a') as f:
    if round == 1:
    f.write("\n-------------------------------------\n")
    f.write(str(accuracy)+" ")

            except FileNotFoundError:
                with open('log.txt', 'w') as f:
                    if round == 1:
                        f.write("\n-------------------------------------\n")
                    f.write(str(accuracy)+" ")
            ```

    ````

3.  Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale est : 63 %

## Exercices:

1.  Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par
    [le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet).

         Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est

    un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit,
    plus les différences dans le nombre d'images entre les clients sont grandes; _n_ est le nombre de client total)

    **Réponse** :

    ```python
    if data_split == "iid":
        props = [1 / num_clients] * num_clients
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
      props = np.random.dirichlet([alpha] * num_clients).round(2)
      datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
    ```

2.  Retester votre code avec _alpha=0.8_

    1. Quelles sont les tailles des jeux de données pour chaque client ?

       **Réponse** : Pour le client 1 : 16200 images pour l'entraînement et 1800 images pour la validation.
       Pour le client 2 : 15300 images pour l'entraînement et 1700 images pour la validation.

    2. Quelle est la précision finale du modèle ?

       **Réponse** : la précision finale du modèle est : 59 %

3.  Lancer votre code suiant:

- Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
- Dans Terminal 3:

  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```

  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?

     **Réponse** : Les données sont distribués par classes.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

     **Réponse** : la précision finale du modèle est : 52 % donc moins bonne et moins performent par rapport aux résultats précédents.

4. Restez le code avec les même commandes sauf round=5, local_epochs=2.

   1. Est-ce que l'execution est plus rapid ? Pourquoi ?

      **Réponse** : L'exécution est just un petit peu plus rapide (environ 5 seconds moins que les résultats précédents pour les phases de tests) principalement parce que le nombre de tours de communication a été réduit, ce qui diminue le temps passé en échanges de données entre le serveur et les clients.

   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

      **Réponse** : la précision finale du modèle est : 48 % donc moins bonne et moins performent par rapport aux résultats précédents.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.

   1. Est-ce que l'execution est plus rapid ? Pourquoi ?

      **Réponse** : Non cette fois elle a pris le même temps que le résultat précédant.

   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

      **Réponse** : la précision finale du modèle est : 41 % donc moins bonne et moins performent par rapport aux résultats précédents.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

   **Réponse** :

```python
if dataset == "CIFAR10":
    trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
    testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
elif dataset == "CIFAR100":
    trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
    testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
else:
    raise NotImplementedError(
        "The dataset is not implemented"
    )
```
