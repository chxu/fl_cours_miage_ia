**Nom**: RIHET

**Prénom**: Nathan

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:
### Jeux de données
1. Quel jeu de données ont été testé ? 

   **Réponse** : CIFAR-10 il me semble vu qu'on import ce package `from torchvision.datasets import CIFAR10` pour charger les données.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
  **Réponse** : 
  Les données sont réparties entre les clients en fonction de l'argument `--node_id` passé lors de l'appel du script `client.py`. Elles sont réparties dans la fonction `prepare_dataset` du fichier `prepare_dataset.py`. La fonction 'load_datasets' divise l'ensemble du dataset en partitions égales pour chaque client en utilisant `random_split` de PyTorch. Les données sont ensuite chargées dans un `DataLoader` pour chaque client. Chaque client utilise 90% de sa partition pour l'entrainement et 10% pour la validation.

  Le nombre d'images pour chaque client est donc de 50000 * 0.9 / 2 = 22500 pour l'entrainement et 50000 * 0.1 / 2 = 2500 pour la validation. Vu qu'il y a 2 clients dans notre cas.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Un modèle de réseau de neurones convolutif (CNN) a été testé étant donné que l'on traite des images.

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle comporte 3 couches de convolution suivies de 3 couches de max pooling. Ensuite, il y a 2 couches entièrement connectées. La structure est la suivante :
   ```python
    class Net(nn.Module):
    def __init__(self) -> None:
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return self.fc3(x)
    ```


### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : L'algorithme d'optimisation utilisé est la descente de gradient stochastique (SGD). On peut trouver cette information dans la fonction `train` du fichier `client.py` où l'on crée un optimiseur de type `torch.optim.SGD`.

    ```python
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    ```

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : 1 époque de calcul local a été effectuée. On peut trouver cette information dans la fonction `train` du fichier `client.py` où l'on boucle sur le nombre d'époques. Nous n'avons pas renseigné le nombre d'époques dans les arguments du script `client.py` donc par défaut, il est égal à 1.

    ```python
    for epoch in range(local_epochs):
    ```

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : 10 tours de communication au total. On peut trouver cette information dans le fichier `server.py` où l'on boucle sur le nombre de tours de communication. Nous n'avons pas renseigné le nombre de tours dans les arguments du script `server.py` donc par défaut, il est égal à 10.

    ```python
    for _ in range(rounds):
    ```

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation est réalisée par le serveur. Les tailles des jeux de données pour l'évaluation sont les mêmes que pour l'entrainement, c'est-à-dire 22500 pour l'entrainement et 2500 pour la validation.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Les résultats de l'évaluation sont enregistrés dans le fichier `log.txt` dans la fonction `log_metrics` du fichier `server.py`.

    ```python
    with open("log.txt", "a") as f:
        f.write(f"{metrics}\n")
    ```



3. Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale du modèle est de 0.6222.

    ```python
    INFO flwr 2024-02-19 10:44:23,906 | app.py:228 | app_fit: metrics_distributed {'accuracy': [(1, {0.4028}), (2, {0.498}), (3, {0.5288}), (4, {0.552}), (5, {0.5848}), (6, {0.583}), (7, {0.6034}), (8, {0.6056}), (9, {0.6138}), (10, {0.6222})]}
    ```
    ```python

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
        elif data_split == "non_iid_number":
            alpha = 0.8
            proportions = torch.tensor(np.random.dirichlet([alpha] * num_clients))

            # Calcul des indices de début pour chaque partition
            total_size = len(trainset)
            indices = torch.randperm(total_size)
            partition_sizes = [int(proportion * total_size) for proportion in proportions]
            # Ajustement pour s'assurer que la somme des partitions correspond à la taille totale
            partition_sizes[-1] = total_size - sum(partition_sizes[:-1])

            # Création des partitions
            datasets = []
            start_index = 0
            for size in partition_sizes:
                end_index = start_index + size
                datasets.append(Subset(trainset, indices[start_index:end_index]))
                start_index = end_index

    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ? (La commande pour tester le code avec alpha=0.8 est `python client.py --node_id 0` et `python client.py --node_id 1`)
       
      **Réponse** : 
      Client 0: Train Size = 2583, Validation Size = 287
      Client 1: Train Size = 42417, Validation Size = 4713

      On constate une répartition très inégale des données entre les clients. Cela est dû à la loi de Dirichlet qui génère des proportions aléatoires. Ici, on a un alpha de 0.8, ce qui signifie que l'on a une grosse disparité entre les proportions générées. On peut voir que le client 0 a très peu de données pour l'entrainement et la validation par rapport au client 1.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : La précision finale du modèle avec alpha=0.8 est de 0.6126217532467533
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --round 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   

      **Réponse** : la distribution des données parmi les clients est gérée selon le paramètre data_split. Pour une distribution IID ("iid"), j'utilise random_split pour diviser de manière équitable l'ensemble de données d'entraînement entre tous les clients, sans tenir compte des classes, ce qui garantit une répartition homogène des données. En mode non-IID par nombre ("non_iid_number"), j'adopte une approche basée sur la loi de Dirichlet pour attribuer de manière aléatoire des quantités variables de données à chaque client, ce qui introduit une disparité dans la taille des ensembles de données des clients. Pour la distribution non-IID par classe ("non_iid_class"), je répartis les données de manière à ce que chaque client reçoive des données de certaines classes uniquement, ce qui crée une hétérogénéité dans la distribution des classes parmi les clients

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : La précision finale du modèle avec alpha=0.8 est de 0.6126217532467533. La précision finale du modèle avec `data_split == "non_iid_number"` est de 0.6222. On constate que la précision finale est légèrement plus élevée avec `data_split == "iid"` soit les paramètres par défaut.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapide ? Pourquoi ? 
        
       **Réponse** :

       L'exécution est sensiblement plus rapide en raison du nombre réduit de rounds de communication entre le serveur et les clients, bien que chaque client effectue plus d'époques localement. Il faut considérer le trade-off entre la communication et le calcul local.


    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

       **Réponse** : La réduction du nombre de rounds de communication a un impact sur la précision finale du modèle. La précision finale du modèle avec `round=5, local_epochs=2` est de 0.4554. On constate que la précision finale est bien plus faible avec moins de rounds de communication.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : L'execution est encore plus rapide en raison du nombre de rounds réduit entre le serveur et les cleints que qu'il y ai plus d'époques localement pour chaque client

    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

       **Réponse** : A la suite de cet entrainement j'ai eu 0.34 en précision ce qui est bien dessous de ma précision précédente. Cela est dû au fait que l'on a fait moins de tours de communication et donc moins de mise à jour du modèle malgré un nombre d'époque cumulé aussi élevé.

6. Compléter le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       ### prepare_dataset.py

        from torchvision.datasets import CIFAR100

        def load_datasets(num_clients: int, dataset, data_split):
    # Download and transform CIFAR-10 (train and test)
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError(
           "The dataset is not implemented"
        )
       ```

       Lorsque l'on lance le code côté client on doit préciser maintenant le dataset que l'on veut utiliser. On peut le faire en ajoutant un argument `--dataset CIFAR100` lors de l'appel du script `client.py`. Comme cela :
       ```bash
        > python client.py --node_id 0 --data_split non_iid_class -local_epochs 1 --dataset CIFAR100
        ```
  
        ```bash