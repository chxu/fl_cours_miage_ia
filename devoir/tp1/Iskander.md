**Nom**: Regaieg

**Prénom**: Iskander

**Date**: 20/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : 
Le jeu de données testé est CIFAR-10, comme spécifié dans le code du fichier prepare_dataset.py. La fonction load_datasets télécharge et transforme le jeu de données CIFAR-10 pour l'entraînement et les tests.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : Les données sont réparties entre les clients selon deux modes : "iid" (répartition équilibrée des données entre les clients) et "non_iid_class" (répartition des données en fonction de la classe entre les clients).

    Dans le code, ces informations sont définies dans la fonction load_datasets du fichier prepare_dataset.py. La répartition des données se fait dans les blocs conditionnels correspondants :

    Pour le mode "iid", la répartition équitable est implémentée en divisant le jeu de données en partitions égales pour simuler différents ensembles de données locaux.
    Pour le mode "non_iid_class", les données sont réparties en fonction des classes entre les clients.
    Le nombre d'images que chaque client utilise pour l'entraînement et la validation dépend du jeu de données spécifique de chaque client. Ces informations peuvent être trouvées dans les fonctions train et test du fichier client.py, où les dataloaders pour l'entraînement et la validation sont utilisés pour itérer sur les données. Les tailles des dataloaders déterminent le nombre d'images utilisées pour l'entraînement et la validation.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Le modèle testé est un réseau de neurones convolutif (CNN) défini dans le fichier client.py. Il s'agit d'un modèle simple composé de deux couches de convolution, suivies chacune d'une couche de max pooling, et de trois couches entièrement connectées (ou linéaires). Ce modèle est utilisé pour effectuer une classification sur le jeu de données CIFAR-10.
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle testé comporte un total de sept couches, réparties comme suit :

Couche de convolution 1 :

Entrée : 3 canaux (pour les images en couleur RGB de CIFAR-10)
Sortie : 6 canaux
Noyau de convolution : 5x5
Fonction d'activation : ReLU
Couche de max pooling 1 :

Taille de la fenêtre : 2x2
Couche de convolution 2 :

Entrée : 6 canaux
Sortie : 16 canaux
Noyau de convolution : 5x5
Fonction d'activation : ReLU
Couche de max pooling 2 :

Taille de la fenêtre : 2x2
Couche entièrement connectée (FC1) :

Nombre de neurones : 120
Fonction d'activation : ReLU
Couche entièrement connectée (FC2) :

Nombre de neurones : 84
Fonction d'activation : ReLU
Couche de sortie (FC3) :

Nombre de neurones : 10 (correspondant au nombre de classes dans CIFAR-10)
Fonction d'activation : Aucune (la couche de sortie est généralement suivie d'une fonction softmax lors de l'entraînement pour obtenir des probabilités de classe)

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :Dans le code, chaque client utilise l'algorithme de descente de gradient stochastique (SGD) pour l'entraînement du modèle local. Cela est implémenté dans la fonction fit de la classe FlowerClient du fichier client.py.

    Voici où vous pouvez trouver cette information dans le code :

    La fonction fit est appelée lors de l'entraînement du modèle local de chaque client. Dans cette fonction, vous pouvez voir que le SGD est utilisé comme optimiseur (torch.optim.SGD) pour mettre à jour les poids du modèle en fonction du gradient de la fonction de perte par rapport aux paramètres du modèle.

    Plus précisément, cette ligne de code montre l'application de l'algorithme SGD dans la fonction fit :

    optimizer = torch.optim.SGD(net.parameters(), lr=0.01, momentum=0.9)
    Cette ligne crée un optimiseur SGD avec un taux d'apprentissage (learning rate) de 0.01 et un moment de 0.9, puis les gradients sont calculés et les poids du modèle sont mis à jour à chaque itération de l'entraînement en utilisant cet optimiseur.
2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Pour chaque client, le nombre d'époques de calcul local effectuées est défini par la variable local_epochs. Dans le code, cette variable est définie dans le fichier client.py et a une valeur par défaut de 1 
    Cette variable est ensuite utilisée dans la définition de la classe FlowerClient pour former et entraîner le modèle local. Dans cette fonction fit, local_epochs est passé en argument à la fonction train pour spécifier le nombre d'époques de calcul local.
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :Le nombre total de tours de communication est égal au nombre de rounds de formation multiplié par le nombre de clients.
    Pour calculer le nombre total de tours de communication, on peut utiliser la formule suivante : total_communication_rounds = server_round * num_clients
    Où num_clients est le nombre total de clients dans le système. Cette variable est définie dans le fichier client.py qui est 2
    et server_round est le nombre de rounds qui est définie dans le fichier server.py qui est 10
    d'ou total_communication_rounds = 10 * 2 =20

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : l'évaluation est réalisée par le serveur, qui récupère les résultats de l'évaluation effectuée par chaque client.

    Pour l'évaluation, chaque client utilise un chargeur de données de validation valloaders[cid] retourné par la fonction load_datasets. La taille de ce chargeur de données de validation dépend de la taille de la partition de validation créée pour chaque client dans la fonction load_datasets.
    Dans la fonction load_datasets, après la division de la partition de données de formation en num_clients parties égales, chaque partition est divisée en une partie de formation et une partie de validation à l'aide de la fonction random_split. La taille de la partie de validation est fixée à 10 % de la taille de la partition de formation.
    Par conséquent, la taille du jeu de données de validation pour chaque client est de 10 % de la taille de la partition de données de formation pour ce client.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :les résultats de l'évaluation sont enregistrés dans le fichier "log.txt" dans la fonction weighted_average dans le fichier server.py.

    Dans cette fonction, les résultats d'évaluation des clients sont traités et enregistrés dans le fichier "log.txt" en utilisant la commande
    with open('log.txt', 'a') as f:
    f.write(str(accuracy)+" ")
    Cette commande ouvre le fichier "log.txt" en mode d'écriture et ajoute la valeur d'exactitude actuelle à la fin du fichier.

3. Quelle est la précision finale du modèle ?

    **Réponse** : 0.612

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python


def load_datasets(num_clients, dataset = "CIFAR10", data_split = "iid"):
    if dataset == "CIFAR10":
        trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform_train)
        testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform_test)
    else:
        raise ValueError("Invalid dataset")

  
    elif data_split == "non_iid_number":
        trainloaders, valloaders = [], []
        alpha = 0.8  # Argument pour controller le niveau de non-iidness
        proportions = np.random.dirichlet([alpha]*num_clients)
        proportions = (np.array(proportions)*len(trainset)).astype(int)
        indices = np.arange(len(trainset))
        for cid in range(num_clients):
            train_indices = np.random.choice(indices, size=proportions[cid], replace=False)
            val_indices = list(set(indices) - set(train_indices))
            trainloaders.append(DataLoader(Subset(trainset, train_indices), batch_size=32))
            valloaders.append(DataLoader(Subset(trainset, val_indices), batch_size=32))
        testloader = DataLoader(testset, batch_size=32)
        return trainloaders, valloaders, testloader
      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : Client 0 : 2453 images
Client 1 : 2547 images
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** :
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Répartition Non-IID basée sur le Nombre d'échantillons : Les données sont distribuées de manière aléatoire parmi les clients en fonction du nombre total d'échantillons. Certains clients peuvent recevoir plus d'échantillons que d'autres, mais chaque échantillon est toujours sélectionné de manière aléatoire dans l'ensemble de données.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : 704 train / 79 test
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Oui, l'exécution plus rapide avec round=5 et local_epochs=2 par rapport aux valeurs précédentes. Cela est dû au fait que nous avons réduit le nombre de tours de 10 à 5, ce qui signifie que le processus global d'entraînement et d'évaluation sera plus court. De plus, en augmentant le nombre d'époques locales (local_epochs) de 1 à 2, chaque client effectuera plus d'itérations d'entraînement lors de chaque tour, ce qui peut conduire à une convergence plus rapide.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Le modèle à une précision inférieur 0.436

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :l'exécution est un peut plus courte en raison de la réduction du nombre de tours , mais chaque client effectue plus nombre d'époches.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :accuaracy modèle global : 0.32 / le modèle a eu moins d'opportunités pour s'améliorer par rapport aux itérations précédentes avec plus de tours. Cependant, chaque client a eu plus de temps pour s'entraîner sur ses données locales, ce qui peut conduire à des modèles locaux plus précis.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** : fichier prepare_dataset.py
       ```python
       import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split, Subset
from torchvision.datasets import CIFAR10, CIFAR100  # Ajout de CIFAR100

def load_datasets(num_clients: int, dataset, data_split):
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":  # Ajout de la condition pour CIFAR100
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError(
           "The dataset is not implemented"
        )

    if data_split == "iid":
        props = [1/num_clients]*num_clients
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        trainloaders, valloaders = [], []
        alpha = 0.8  
        proportions = np.random.dirichlet([alpha]*num_clients)
        proportions = (np.array(proportions)*len(trainset)).astype(int)
        indices = np.arange(len(trainset))
        for cid in range(num_clients):
            train_indices = np.random.choice(indices, size=proportions[cid], replace=False)
            val_indices = list(set(indices) - set(train_indices))
            trainloaders.append(DataLoader(Subset(trainset, train_indices), batch_size=32))
            valloaders.append(DataLoader(Subset(trainset, val_indices), batch_size=32))
        testloader = DataLoader(testset, batch_size=32)
        return trainloaders, valloaders, testloader
    elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)
        class_per_client = num_classes/num_clients
        data_indices_each_client = {client: [] for client in range(num_clients)}
        for c in range(num_classes):
            indices = (torch.tensor(trainset.targets)[..., None] == c).any(-1).nonzero(as_tuple=True)[0]
            client_belong = int(c/class_per_client)
            data_indices_each_client[client_belong].extend(list(indices))
        datasets = []
        for i in range(num_clients):
            datasets.append(Subset(trainset, data_indices_each_client[i]))
    else:
        raise NotImplementedError(
           "The data split is not implemented"
        )

    trainloaders = []
    valloaders = []
    for ds in datasets:
        len_val = len(ds) // 10  
        len_train = len(ds) - len_val
        lengths = [len_train, len_val]
        ds_train, ds_val = random_split(ds, lengths, torch.Generator().manual_seed(42))
        trainloaders.append(DataLoader(ds_train, batch_size=32, shuffle=True))
        valloaders.append(DataLoader(ds_val, batch_size=32))
    testloader = DataLoader(testset, batch_size=32)
    return trainloaders, valloaders, testloader

      
   
       ```