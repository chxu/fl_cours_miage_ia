**Nom**: SAHLI 

**Prénom**: MOOTEZ

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** :CIFAR-10

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :
    Les données sont réparties entre les clients de trois manières possibles, comme indiqué dans la fonction `load_datasets` du fichier `prepare_dataset.py` :

      1. **IID (Independently and Identically Distributed)**: Les données sont divisées de manière uniforme entre les clients, c'est-à-dire que chaque client reçoit une partie des données qui est un échantillon représentatif de l'ensemble du dataset. Ceci est réalisé en divisant le set d'entraînement en partitions égales (`props = [1/num_clients]*num_clients`).

      2. **Non-IID par nombre**: Cette option n'est pas implémentée dans le code fourni (`# TO IMPLEMENT`), mais elle viserait à distribuer les données de manière non uniforme entre les clients, par exemple, en donnant un nombre différent d'images à chaque client.

      3. **Non-IID par classe**: Les données sont réparties de sorte que chaque client reçoit des images de seulement quelques classes. Le nombre de classes par client est déterminé par `num_classes / num_clients`, et les données sont assignées en conséquence (`data_indices_each_client[client_belong].extend(list(indices))`).

      ```python
      if data_split == "iid":
      # Split training set into `num_clients` partitions to simulate different local datasets
        props = [1/num_clients]*num_clients
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
      elif data_split == "non_iid_number":
      # TO IMPLEMENT
        pass
      elif data_split == "non_iid_class":
        num_classes = len(trainset.classes)

      ```

      Pour  le nombre d'images utilisées cela dépend de la répartition initiale. Une fois les données divisées selon l'une des stratégies ci-dessus, 10 % de l'ensemble d'entraînement de chaque client sont utilisés pour la validation (`len_val = len(ds) // 10`). Le nombre exact d'images pour l'entraînement (`len_train = len(ds) - len_val`) et pour la validation (`len_val`) dépend de la taille initiale de l'ensemble de données attribué à chaque client selon la méthode de répartition choisie.



### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : - Le modèle testé est un réseau de neurones convolutif (CNN), conçu pour le traitement d'images. Ce type de modèle est couramment utilisé pour des tâches de vision par ordinateur telles que la classification d'images.
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :Le modèle comporte cinq couches principales, dont la structure est détaillée comme suit​​ :
      2 couches convolutives :
        conv1: Convolution avec 3 canaux d'entrée, 6 canaux de sortie et un noyau de taille 5.
        conv2: Convolution avec 6 canaux d'entrée, 16 canaux de sortie et un noyau de taille 5.
      1 couche de pooling :
        pool: Pooling maximal avec une fenêtre de taille 2x2.
      3 couches entièrement connectées (dense ou linéaires) :
        fc1: Linéaire avec 1655 unités d'entrée (correspondant à la dimensionnalité des caractéristiques extraites par les couches convolutives et le pooling) et 120 unités de sortie.
        fc2: Linéaire avec 120 unités d'entrée et 84 unités de sortie.
        fc3: Linéaire avec 84 unités d'entrée et 10 unités de sortie, correspondant aux classes du dataset CIFAR-10.
      Chaque couche convolutive est suivie d'une fonction d'activation ReLU et d'une opération de pooling. Les couches entièrement connectées sont également suivies d'activations ReLU, à l'exception de la dernière couche qui sert à la classification finale.

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :
    Pour l'entraînement du modèle local, chaque client utilise l'algorithme de descente de gradient stochastique (SGD) avec une perte de type cross-entropy. Cette information peut être trouvée dans la fonction train du fichier client.py, qui est appelée dans la méthode fit de la classe FlowerClient
2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Le nombre d'époques de calcul local pour chaque client est déterminé par l'argument --local_epochs, qui est passé en ligne de commande lors de l'exécution du script client.py
3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est défini par l'argument --round dans le script server.py. Par défaut, il est défini sur 10 tours, mais cela peut être ajusté en passant une valeur différente en ligne de commande lors de l'exécution du serveur​

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** :
    tailles des jeux de données pour l'évaluation est de 79 

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :Les résultats de l'évaluation sont enregistrés dans "log.txt", comme indiqué dans le fichier server.py. Après chaque tour, le serveur enregistre la précision globale obtenue dans ce fichier.

3. Quelle est la précision finale du modèle ?

    **Réponse** :La précision finale du modèle, telle que rapportée dans le fichier log.txt que vous avez fourni, est de 0.6342. Cette valeur reflète la performance du modèle sur l'ensemble des données d'évaluation à la fin du dernier tour d'apprentissage fédéré.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    elif data_split == "non_iid_number":
        alpha = 0.8  # Paramètre alpha pour la loi de Dirichlet
        proportions = np.random.dirichlet([alpha]*num_clients)
        
        total_size = len(trainset)
        indices = np.random.permutation(total_size)
        
        # Calcul des indices de départ pour chaque partition
        start = 0
        datasets = []
        for prop in proportions:
            end = start + int(prop * total_size)
            datasets.append(Subset(trainset, indices[start:end]))
            start = end
      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** :
      pour le client 0, on voit "Testing: 100%" suivi de "39/39", ce qui indique que le client 0 a utilisé 39 exemples pour l'évaluation. De même, pour le client 1, on voit "Testing: 100%" suivi de "138/138", indiquant que le client 1 a utilisé 138 exemples pour l'évaluation. Cela implique que la taille des jeux de données de validation pour le client 0 est de 39 images et pour le client 1 est de 138 images.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** :la précision finale du modèle est de 0.6342
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --round 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :
      Les données sont distribuées parmi les clients selon une répartition non-IID par classe, c'est-à-dire que chaque client reçoit des données qui ne sont pas réparties de manière identique et indépendante. Dans un tel scénario, chaque client peut avoir des images de seulement quelques classes, rendant la distribution des données déséquilibrée entre les clients et simulant une situation réelle où les données peuvent être intrinsèquement non uniformes.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** :
     la précision finale du modèle après dix tours avec une répartition non-IID par classe est de 0.4872. Ce résultat est plus bas que la précision finale obtenue avec une répartition IID ou non-IID par nombre, qui était de 0.6342 dans le fichier log précédent. Cela peut indiquer que le modèle a plus de difficultés à apprendre à partir d'un ensemble de données non-IID par classe, ce qui est cohérent avec le fait que ce type de répartition peut rendre l'apprentissage plus difficile en raison de la distribution déséquilibrée des classes entre les clients.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
       **Réponse** :
       oui l'executiont est plus rapide , Moins de tours signifie moins d'itérations globales d'entraînement et d'évaluation
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
       a précision finale du modèle après 5 tours et 2 époques locales est de 0.4776. Si cette précision est inférieure à celle obtenue dans les exécutions précédentes avec un nombre plus élevé de tours ou d'époques, cela peut indiquer que moins de tours de communication et moins d'époques locales ne suffisent pas pour que le modèle converge vers une solution optimale

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
       **Réponse** : oui car on passe plus de temps sur l'entraînement local (5) mais on a moin de tours 
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :
       La précision finale du modèle après 2 tours et 5 époques locales est de 0.4044. Cela peut suggérer que même avec plus d'époques locales pour l'entraînement, la réduction drastique du nombre de tours globaux peut ne pas permettre une convergence adéquate du modèle ou une généralisation suffisante

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       def load_datasets(num_clients: int, dataset, data_split):
      
        transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        )
        if dataset == "CIFAR10":
            trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
            testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
        elif dataset == "CIFAR100":
            trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
            testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
        else:
            raise NotImplementedError(
              "The dataset is not implemented"
            )
            ...
   
       ```