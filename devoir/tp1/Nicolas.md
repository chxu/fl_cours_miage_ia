**Nom**: AVRIL

**Prénom**: Nicolas

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : Le jeu de données utilisé est CIFAR-10. Ce jeu de données comprend des images réparties en 10 classes, comme spécifié dans le script prepare_dataset.py

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : Les données peuvent être réparties de manière identique (iid) entre les clients ou de manière non identique (non-iid), soit en nombre d'images (non encore implémenté dans le code) soit par classe. La répartition des données est définie par le paramètre data_split dans prepare_dataset.py. Ce script montre la logique de partitionnement des données pour le mode iid et non-iid par classe, où chaque client reçoit des images de certaines classes uniquement.
    Le script prepare_dataset.py divise chaque partition de données en un ensemble d'entraînement et un ensemble de validation, où 10 % des données de chaque partition sont utilisées pour la validation (ligne 47). Le nombre exact d'images dépend de la répartition initiale des données et du nombre total de données dans le jeu CIFAR-10. Les chargeurs de données créés pour l'entraînement et la validation ont une taille de lot de 32​.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Un modèle de réseau de neurones convolutionnel (ConvNet) a été testé, comme défini dans la classe Net du script client.py

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Le modèle comporte plusieurs couches, dont:
  2 couches convolutionnelles (conv1 et conv2).
  Une couche de pooling (pool) appliquée après chaque couche convolutionnelle.
  3 couches entièrement connectées (fc1, fc2, fc3).
  La structure du modèle est la suivante: entrée → conv1 → pool → conv2 → pool → fc1 → fc2 → fc3 → sortie​

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :L'algorithme d'optimisation utilisé est SGD (Descente de Gradient Stochastique) avec un taux d'apprentissage de 0.01 et un momentum de 0.9. Cette information est trouvée dans la fonction train du script client.py

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Le nombre d'époques locales (local_epochs) est défini par un argument passé au script client.py. Ce nombre peut être configuré lors de l'exécution du script et est utilisé dans la fonction fit du client Flower pour déterminer combien de fois l'ensemble de données local est parcouru pendant l'entraînement

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est défini par l'argument --round dans le script server.py. Par défaut, ce nombre est fixé à 10. Cette configuration détermine combien de fois les clients communiquent avec le serveur pour mettre à jour le modèle global basé sur l'apprentissage local​

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation est réalisée par les clients. Chaque client utilise sa propre fonction evaluate pour tester le modèle local sur son ensemble de validation. La taille des jeux de données pour l'évaluation dépend de la division initiale du jeu de données CIFAR-10 et de la répartition entre les clients. Typiquement, 10 % de chaque partition de données d'un client est réservée pour la validation, comme défini dans prepare_dataset.py. Les chargeurs de données de validation (valloader) sont utilisés dans cette fonction evaluate, avec une taille de lot de 32

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Les résultats de l'évaluation sont enregistrés dans le fichier "log.txt" par le serveur. Cela est fait dans la fonction weighted_average du script server.py, où l'exactitude moyenne pondérée globale est calculée et écrite dans "log.txt" à chaque round de communication. La précision est calculée en multipliant l'exactitude de chaque client par le nombre d'exemples utilisés, puis en prenant la moyenne pondérée. Ce calcul est effectué après chaque round de communication entre les clients et le serveur

3. Quelle est la précision finale du modèle ?

    **Réponse** : La précision finale du modèle, après tous les rounds d'apprentissage fédéré, est de 62.72 %, comme indiqué dans le fichier "log.txt". Cette valeur représente l'exactitude moyenne pondérée globale sur l'ensemble des clients après le dernier round de communication.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    elif data_split == "non_iid_number":
      alpha = 0.5  
      proportions = numpy.random.dirichlet([alpha]*num_clients)
      total_size = len(trainset)
      sizes = [int(p * total_size) for p in proportions]
      sizes[-1] = total_size - sum(sizes[:-1])
      datasets = random_split(trainset, sizes, torch.Generator().manual_seed(42))
      
   
    ```
2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** :  Pour le client0, le jeu de données contient 299 éléments, tandis que pour le client1, il en contient 203.
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** :  La précision atteinte par le modèle est de 0.55
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** :La répartition des données entre les clients est effectuée de manière non IID par classe, attribuant à chaque client des classes spécifiques avec un total de 5 classes différentes par client, basé sur la distribution initiale des classes dans le dataset.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : Les performances du modèle restent similaires aux essais antérieurs, avec une précision finale de 0.53
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : La durée d'exécution reste similaire à celle des essais précédents. Bien que le nombre de rounds soit réduit de moitié, augmentant ainsi les épisodes locaux par deux résulte en un nombre identique d'époques totales par client. La réduction des tours de communication pourrait normalement réduire le temps, mais puisque l'exécution se déroule sur une seule machine, l'impact temporel de la communication est minime.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : La performance du modèle demeure inchangée avec une précision de 0.53.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : La performance diminue : 0.39

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       from torchvision.datasets import CIFAR10, CIFAR100
       elif dataset == "CIFAR100":
          trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
          testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
       ```