**Nom**:

**Prénom**:

**Date**:

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ?

   **Réponse** :
    Le jeu de données qui a été testé estr cifar10.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :
    La répartition des données entre les clients est gérée par la fonction load_datasets dans prepare_dataset.py. 
    Le script divise le jeu de données CIFAR-10 en fonction du nombre de clients et du type de répartition spécifié (par exemple, IID ou non-IID). 
    Le nombre précis d'images utilisées pour l'entraînement et la validation par chaque client dépend de la répartition spécifiée dans le code.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** :
    Un modèle de réseau de neurones convolutifs (CNN) a été testé. 
    La structure du modèle est définie dans client.py et comprend des couches convolutives et entièrement connectées

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** :
    Le modèle comprend plusieurs couches : deux couches convolutives (conv1 et conv2), une couche de pooling (pool), et trois couches entièrement connectées (fc1, fc2, et fc3). La structure détaillée est la suivante :
    Couche convolutive 1 : Convolution avec 3 canaux d'entrée, 6 canaux de sortie, et un noyau de taille 5.
    Pooling : Pooling max avec une fenêtre de taille 2x2.
    Couche convolutive 2 : Convolution avec 6 canaux d'entrée, 16 canaux de sortie, et un noyau de taille 5.
    Couche entièrement connectée 1 : Entrée de taille 1655, sortie de taille 120.
    Couche entièrement connectée 2 : Entrée de taille 120, sortie de taille 84.
    Couche entièrement connectée 3 (sortie) : Entrée de taille 84, sortie de taille 10 (classes).


### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** :
    L'algorithme d'entraînement utilisé est la descente de gradient stochastique (SGD) avec un taux d'apprentissage de 0.01 et un momentum de 0.9, comme défini dans la fonction train du script client.py.

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** :
    Le nombre d'époques pour l'entraînement local est spécifié comme argument de la fonction train dans client.py. Ce nombre peut varier selon les paramètres passés lors de l'exécution du script.

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
    Le nombre total de tours de communication est spécifié par un argument de ligne de commande dans server.py (--round), avec une valeur par défaut de 10.

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** :
   L'évaluation est réalisée par les clients, comme indiqué par les logs [Client 0, round 1] evaluate et [Client 1, round 1] evaluate. 
   La taille spécifique des jeux de données pour l'évaluation n'est pas mentionnée dans les logs, mais généralement, dans l'apprentissage fédéré, chaque client utilise son propre jeu de données local pour l'évaluation.

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** :
    Les résultats de l'évaluation sont enregistrés dans le fichier "log.txt" par la fonction log dans le script server.py (ligne 29)


3. Quelle est la précision finale du modèle ?

    **Réponse** :
    La précision finale du modèle, après 10 tours, est de 0.6398 ou 63.98%, comme indiqué dans les logs du serveur.

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :

    ```python
            def load_datasets_non_iid_dirichlet(num_clients: int, alpha: float):
            transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
            dataset = CIFAR10(root="./dataset", train=True, download=True, transform=transform)
            num_items = len(dataset) // num_clients
            dirichlet_dist = np.random.dirichlet([alpha] * num_clients)
            indices = np.random.permutation(len(dataset))
            datasets = []
            current_index = 0
            for proportion in dirichlet_dist:
                num_items_i = int(proportion * len(dataset))
                indices_i = indices[current_index:current_index + num_items_i]
                dataset_i = Subset(dataset, indices_i)
                datasets.append(DataLoader(dataset_i, batch_size=64, shuffle=True))
                current_index += num_items_i
            return datasets
    ```
   

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** :
        Les tailles des jeux de données pour chaque client dépendent des proportions générées par la distribution de Dirichlet. 
        Étant donné que ces proportions sont déterminées aléatoirement à chaque exécution (selon le paramètre alpha et le nombre de clients), les tailles exactes varieront. 
        Pour obtenir les tailles spécifiques, vous devrez exécuter le code et enregistrer les tailles des sous-ensembles attribués à chaque client. 
        Cela peut être fait en ajoutant des instructions d'impression dans la boucle qui crée datasets dans load_datasets_non_iid_dirichlet.
   
      2. Quelle est la précision finale du modèle ? 
   
         **Réponse** :
            La précision finale du modèle, après avoir retesté votre code avec la distribution non_iid_number utilisant la loi de Dirichlet avec alpha=0.8, est de 61.58% (précision du test global au round 10).
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Les données sont distribuées en se basant sur des classes spécifiques à chaque client, créant une distribution non-IID parmi les clients.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : La précision du modèle s'améliore progressivement à travers les tours d'apprentissage, démontrant l'efficacité de l'apprentissage fédéré même avec une distribution non-IID des données.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Oui, l'exécution est plus rapide car le nombre de rounds est réduit de 10 à 5, et bien que chaque client effectue plus d'époques locales (de 1 à 2), le nombre total de communications entre clients et serveur est diminué, ce qui réduit le temps d'exécution global.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Avec moins de rounds mais plus d'époques locales par round, le modèle a potentiellement une meilleure chance d'apprendre des caractéristiques plus complexes dans chaque client avant l'agrégation. Cela pourrait conduire à une amélioration de la précision plus rapide par round, mais la précision finale pourrait être impactée par le nombre réduit de cycles d'agrégation globaux.
   

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Oui, l'exécution devient plus rapide car le nombre total de rounds est réduit à 2, et malgré l'augmentation des époques locales à 5 par client, le nombre global de communications serveur-client diminue. Cela réduit le temps d'exécution global en concentrant l'apprentissage au niveau local et en minimisant les étapes d'agrégation.
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : vec moins de rounds mais plus d'époques locales, le modèle peut ne pas bénéficier pleinement de l'ajustement et de l'amélioration à travers les cycles d'agrégation. La précision finale peut ne pas atteindre celle des scénarios avec plus de rounds, reflétant une convergence potentiellement moins efficace due à un ajustement global limité.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
        from torchvision.datasets import CIFAR10, CIFAR100
       def load_datasets(num_clients: int, dataset_name, data_split):
    # Define transformations
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    # Load the specified dataset
    if dataset_name == "CIFAR10":
        trainset = CIFAR10(root='./dataset', train=True, download=True, transform=transform)
        testset = CIFAR10(root='./dataset', train=False, download=True, transform=transform)
    elif dataset_name == "CIFAR100":
        trainset = CIFAR100(root='./dataset', train=True, download=True, transform=transform)
        testset = CIFAR100(root='./dataset', train=False, download=True, transform=transform)
    else:
        raise ValueError("Unsupported dataset")
      
   
       ```