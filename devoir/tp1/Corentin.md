**Nom**: LIZY

**Prénom**: Corentin

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré

## Installation par conda :

- Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
- [Installation de pytorch](https://pytorch.org/)
- Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:

- Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
- Dans Terminal 1:
  ```bash
  > python server.py
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node-id 0
  ```
- Dans Terminal 3:
  ```bash
  > python client.py --node-id 1
  ```

## Questions:

### Jeux de données

1.  Qeul jeu de données ont été testé ?

    **Réponse** : C'est le dataset CIFAR10.

2.  Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ?
    Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
        **Réponse** : Les données sont réparties de manière "iid" entre les clients, comme indiqué par l'argument --data_split="iid" dans les paramètres du script client. Cette répartition est gérée par la fonction prepare_dataset.get_data_loader appelée dans la classe FlowerClient. Le nombre d'images utilisées pour l'entraînement et la validation n'est pas spécifié directement dans le code fourni, mais dépend de la manière dont prepare_dataset.get_data_loader divise le dataset CIFAR10.

### Modèle d'entraînement

1. Quel type de modèle a été testé ?

   **Réponse** : Un modèle de réseau de neurones convolutifs (CNN).

2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?

   **Réponse** : Le modèle comporte 5 couches principales : deux couches convolutionnelles (conv1 et conv2), suivies chacune par une opération de pooling (pool), et trois couches entièrement connectées (fc1, fc2, fc3).

### Le processus d'entraînement

1.  Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ?
    Où pouvons-nous trouver cette information dans le code ?

        **Réponse** : L'algorithme d'optimisation utilisé est SGD (Descente de Gradient Stochastique) avec un taux d'apprentissage de 0.01 et un momentum de 0.9, comme indiqué dans la fonction train.

2.  Pour chaque client, combien d'époques de calcul local ont été effectuées ?
    Où pouvons-nous trouver cette information dans le code ?  
    Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une _époque_.

        **Réponse** :  Le nombre d'époques de calcul local est défini par l'argument --local_epochs, qui a une valeur par défaut de 1.

3.  Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre total de tours de communication est défini par l'argument --round dans le script du serveur, avec une valeur par défaut de 10.

### Evaluation

1.  Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ?
    Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : L'évaluation est réalisée par les clients, qui testent le modèle sur leurs propres jeux de données de validation locaux et renvoient les résultats au serveur. Pour l'évaluation le dataset a une taille de 2500.

2.  Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
    Où nous pouvons trouver cette information dans le code ?

        **Réponse** :  Les résultats de l'évaluation sont enregistrés dans "log.txt" dans la fonction weighted_average, qui est appelée après chaque round d'évaluation pour calculer la moyenne pondérée des précisions rapportées par les clients et imprimer cette précision globale.

3.  Quelle est la précision finale du modèle ?

    **Réponse** : Le modèle fini à 0.6144 de précision

## Exercices:

1.  Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par
    [le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet).

        Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est

    un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit,
    plus les différences dans le nombre d'images entre les clients sont grandes; _n_ est le nombre de client total)

        **Réponse** :
        ```python
        prepare_dataset.py
        elif data_split == "non_iid_number":
          alpha = 0.8
          proportions = np.random.dirichlet([alpha] * num_clients)
          num_samples = torch.tensor([int(len(trainset) * prop) for prop in proportions])
          num_samples[-1] = len(trainset) - num_samples[:-1].sum()
          datasets = random_split(trainset, num_samples.tolist(), torch.Generator().manual_seed(42))
          pass
        ```

2.  Retester votre code avec _alpha=0.8_

    1. Quelles sont les tailles des jeux de données pour chaque client ?

       **Réponse** : Client 1: 40433 entrainement, 4492 validation
       Client 2: 4568 entrainement, 507 validation

    2. Quelle est la précision finale du modèle ?

       **Réponse** : Le modèle fini à 0.7057761732851986 de précision

3.  Lancer votre code suiant:

- Dans Terminal 1:
  ```bash
  > python server.py --round 10
  ```
- Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```
- Dans Terminal 3:

  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```

  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?  
     **Réponse** : avec --data_split non_iid_class, les données sont réparties entre les clients de manière à ce que chaque client reçoive uniquement des échantillons d'un sous-ensemble spécifique de classes, ce qui crée une distribution non IID parmi les clients.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
     **Réponse** : 0.51 de précision. Il n y a pas de grandes différences avec l'exécution d'avant. Si ce n'est que les clients prennent le même temps à l'éxecution, avant le client 2 était plus rapide car il avait un jeu de données moins conséquent.

4. Restez le code avec les même commandes sauf round=5, local_epochs=2.

   1. Est-ce que l'execution est plus rapid ? Pourquoi ?

      **Réponse** : Non, il y a moins de round mais le client effectue deux trainings par round donc ça revient au même. Sachant que le test après chaque round prend 0s.

   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

      **Réponse** : Pas énormément de différences, le modèle converge un peu plus vite (0.39 au round 1 contre 0.27) car plus de training par round. Mais vu qu'il est moins testé il ne monte pas très haut en accuracy.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.

   1. Est-ce que l'execution est plus rapid ? Pourquoi ?

      **Réponse** : Non pour la même raison qu'avant. Par rapport à l'éxecution 1, il y a 5 fois moins de round mais 5x plus de training par round. Sachant que le test après chaque round prend 0s.

   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?

      **Réponse** : L'entrainement n'est pas bon du tout, il n y a absolument pas assez de tests donc le modèle ne converge pas et il fini à 0.37 pour le même temps d'execution. Alors qu'il finissait bien plus haut pour round = 5, local_epochs = 2.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

   **Réponse** :

   ```python
    prepare_dataset.py

    elif dataset == "CIFAR100":
     trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
     testset = CIFAR100("./dataset", train=False, download=True, transform=transform)


   ```
