**Nom**: BOUKADIDA

**Prénom**: Rami

**Date**: 19/02/2023

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Quel jeu de données ont été testé ? 

   **Réponse** : CIFAR-10


2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** : Les données sont répartis selon le param `data-split` (idd ou non-idd) Si le data split est idd alors le dataset est diviser en part egal. Sinon, chaque client se voit attribuer une part aléatoire.
Chaque client utilise 704 images pour l'entraînement et 79 images pour la validation. On retrouve ces informations dans les fonctions test et train du fichier `client.py`, plus précisément au niveau de la boucle tqdm.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Un CNN


2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : Ce modèle comporte un total de sept couches, réparties comme suit :
    
       1. Couche de convolution : `self.conv1`
       2. Couche de pooling : `self.pool` (utilisée deux fois)
       3. Couche de convolution : `self.conv2`
       4. Couche entièrement connectée (fully connected) : `self.fc1`
       5. Couche entièrement connectée (fully connected) : `self.fc2`
       6. Couche entièrement connectée (fully connected) : `self.fc3`
       7. Couche de fonction d'activation (relu) : utilisée à chaque fois après l'application des couches linéaires (fully connected) et convolutives.

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : On retrouve cette information dans la fonction train du fichier `client.py`. L'algorithme utilisé est le SGD


2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : Le nombre d'époques local est par défaut, 1. On retrouve cette information dans le fichier `client.py` au niveau de la variable local_epochs


3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Le nombre de rounds est 10 par défaut. On retrouve cette information dans le fichier `server.py` au niveau de la variable rounds


### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?

    **Réponse** : L'évaluation du modèle est faite en local par le client, on peut verifier cela dans la fonction test du fichier `client.py`.


2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : On peut retrouver cela dans la fonction weighted_average du fichier `server.py`. Plus précisément :
    ```
    try:
        with open('log.txt', 'a') as f:
            if round == 1:
                f.write("\n-------------------------------------\n")
            f.write(str(accuracy)+" ")
   
    except FileNotFoundError:
        with open('log.txt', 'w') as f:
            if round == 1:
                f.write("\n-------------------------------------\n")
            f.write(str(accuracy)+" ")
    ```

3. Quelle est la précision finale du modèle ?

    **Réponse** : 62%

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    if data_split == "iid":
        # Split training set into `num_clients` partitions to simulate different local datasets
        props = [1 / num_clients] * num_clients
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
    elif data_split == "non_iid_number":
        props = np.random.dirichlet([alpha] * num_clients).round(2)
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
      
   
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : client 1 (train = 577 , test = 65) -- client 2 (train =29 , test = 4)
    
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.70
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0 --data_split non_iid_class --local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1 --data_split non_iid_class --local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Les données sont distribués par classes. Par exemple, si mon algo doit reconnaitre 6 classe. Je peux train mon client 1 sur 3 classes et mon client 2 sur les 3 restantes.

   2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
      **Réponse** : Le modèle a une moins bonne accuracy (52%)
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ?
   
       **Réponse** : Non car le nombre d'epoque total resete le meme, l'ordinateur calcule 10 epochs
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Il reste à 52%
   

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ?
   
       **Réponse** : Non car le nombre d'epoque total resete le meme, l'ordinateur calcule 10 epochs
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : L'accuracy chute à 39%


6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

    ```python
    if dataset == "CIFAR10":
        trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
    else:
        raise NotImplementedError(
            "The dataset is not implemented"
        )
    ```