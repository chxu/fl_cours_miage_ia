**Nom**: BENKHADDA

**Prénom**: Mohamed Yassine

**Date**: 19/02/2024

#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** : CIFAR-10

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :  
    
    - la fonction 'load_datasets' du fichier `prepare_dataset.py` divise l'ensemble d'entraînement en num_clients partitions avec des proportions égales, simulant ainsi différents ensembles de données locaux.

    - Pour chaque client 22500 images pour l'entrainement, 2500 pour la validation


### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : Modele CNN
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : 
   
  - **Convolution 1 :** - Entrée : 3 canaux (couleurs RGB) - Sortie : 6 canaux - Filtre : 5x5

  - **Pooling :** - Type : Max pooling - Fenêtre : 2x2 - Décalage : 2

  - **Convolution 2 :** - Entrée : 6 canaux - Sortie : 16 canaux - Filtre : 5x5

  - **Couche Fully Connected 1 :** - Entrée : 16 * 5 * 5 neurones (résultat des couches précédentes) - Sortie : 120 neurones

  - **Couche Fully Connected 2 :** - Entrée : 120 neurones - Sortie : 84 neurones

  - **Couche Fully Connected 3 (Sortie) :** - Entrée : 84 neurones - Sortie : 10 neurones

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : FedAvg dans le parametre 'startegy' du fichier `server.py`

2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : 1 sur le fichier `client.py` la valeur par défaut du epochs est 1 et vu que nous nous n'avons pas changer le parametre lors de l'execution nous avons 1 epoch

3. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

    **Réponse** : la valeur par defaut dans le fichier `server.py` est 10

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    **Réponse** : par le client, 10% du dataset est pour l'evaluation

2. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Dans la fonction 'weighted_average' du fichier `server.py`

3. Quelle est la précision finale du modèle ?

    **Réponse** : 0.6122

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
    #(Veuillez copie-coller votre code ici 
      
   elif data_split == "non_iid_number":
      props = np.random.dirichlet([alpha]*num_clients)
      datasets = random_split(trainset, props, torch.Generator().manual_seed(42))


    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : Client 0 : 44782, client 1 : 5218 (90% pour entrainement et 10% pour validation)
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 0.6215131446919526
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : ils sont distribuees de facon non_iid_class,  Il calcule le nombre de classes par client et initialise une structure pour stocker les indices des échantillons de données détenus par chaque client.

  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : des resultats moins bonnes que celles du 1er modele 
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : l'execution est rapide vu que la communication entre les clients est diminuee en 5 seulement au lieu de 10
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : les resultats sont diminuees vu que la diminution de R entraine une baisse de performance

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Cette methode est plus rapide que les autres vu que le R diminue
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : resultats mediocres par rapport aux autres résultats

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

    **Réponse** :

  ```
  from torchvision.datasets import CIFAR100

  if dataset == "CIFAR10":
    trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
    testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
  elif dataset == "CIFAR100":
        trainset = CIFAR100("./dataset", train=True, download=True, transform=transform)
        testset = CIFAR100("./dataset", train=False, download=True, transform=transform)
  else:
        raise NotImplementedError(
          "The dataset is not implemented"
        )

  ```