Fichier pour [les notes](https://unice.sharepoint.com/:x:/r/sites/GUFederatedLearningcours/_layouts/15/Doc2.aspx?action=edit&sourcedoc=%7Bb49558ee-9918-49a6-9827-59cd8d5afed1%7D&wdOrigin=TEAMS-MAGLEV.teamsSdk_ns.rwc&wdExp=TEAMS-TREATMENT&wdhostclicktime=1708511528253&web=1)


#TP1 : Introduction de l'apprentissage fédéré
## Installation par conda :
* Initiliser l'environnement conda (vous pourriez réutiliser l'environnement dans le cours précédent qui a installé PyTorch)
* [Installation de pytorch](https://pytorch.org/)
* Installation de paquet Flower
  ```bash
  > conda config --add channels conda-forge
  > conda config --set channel_priority strict
  > conda install flwr
  > pip install tqdm
  ```

## Testez:
* Ouvrez trois terminaux dans PyCharm et bien suivez les étapes suivantes :
  ```bash
  > cd TP1_flower_pytorch
  ```
* Dans Terminal 1:
  ```bash
  > python server.py
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node_id 0
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node_id 1
  ```

## Questions:
### Jeux de données
1. Qeul jeu de données ont été testé ? 

   **Réponse** :
    CIFAR10.  CIFAR-10 est composé de 60 000 images couleur en haute résolution (32x32 pixels), réparties en 10 classes distinctes. 
Chaque classe contient 6 000 images. Les 10 classes sont : avions, automobiles, oiseaux, chats, cerfs, chiens, grenouilles, chevaux, navires et camions.

2. Comment les données sont-elles réparties entre les clients ? Où pouvons-nous trouver cette information dans le code ? 
Combien d'images chaque client utilise-t-il pour l'entraînement et la validation ?
  
    **Réponse** :
Chaque client possède un pourcentage de 1/n des données, où n représente le nombre de clients, tiré au hasard parmi toutes les images d'entraînement dans CIFAR10 (50000 images). Pour le cas où n = 2, chaque client dispose de 25000 images, dont 90% sont utilisées pour l'entraînement et 10% pour la validation.

### Modèle d'entraînement
1. Quel type de modèle a été testé ? 

   **Réponse** : CNN
2. Combien de couches comporte ce modèle et veuillez détailler sa structure ?
  
   **Réponse** : 2 couches convolutif et 3 couches linéaires

### Le processus d'entraînement
1. Pour chaque client, quel algorithme ont-ils appliqué pour l'entraînement du modèle local ? 
Où pouvons-nous trouver cette information dans le code ?

    **Réponse** : SGD avec momentum. `    optimizer = torch.optim.SGD(net.parameters(), lr=0.01, momentum=0.9)`
2. Pour chaque client, combien d'époques de calcul local ont été effectuées ? 
Où pouvons-nous trouver cette information dans le code ?   
Remarque : Quand les jeux de données a été parcouru complètement une fois, on appelle cela une *époque*. 

    **Réponse** : 1 époque par défault `local_epochs = parser.parse_args().local_epochs`
   1. Combien des tours de communication au total ? Où nous pouvons trouver cette information dans le code ?

     **Réponse** : 10 tours par défault dans les arguments
      `rounds = parser.parse_args().round`
      `config=fl.server.ServerConfig(num_rounds=rounds),`

### Evaluation
1. Comment l'évaluation est-elle réalisée (par les clients ou par le serveur) ? 
Quelles sont les tailles des jeux de données pour l'évaluation ?
    
    **Réponse** : par le client sur les donnnées de validation. 10% * 25000 = 2500 images.
La précision est ensuite envoyée au serveur, qui calcule une moyenne pondérée de toutes les précisions reçues.

3. Les résultats de l'évaulation sont entregistrés dans le fichier "log.txt".  
Où nous pouvons trouver cette information dans le code ?

    **Réponse** : Dans la fonction `weighted_average(metrics: List[Tuple[int, Metrics]]):` de fichier `server.py`

4. Quelle est la précision finale du modèle ?

    **Réponse** : 

## Exercices:
1. Ajouter une option pour la distribution de jeux de données (`data_split=="non_iid_number"`)où le pourcentage de nombre des images est tiré par 
[le loi de Dirichet](https://fr.wikipedia.org/wiki/Loi_de_Dirichlet). 

    Remarque : Vous pouvez utiliser la fonction fournie par numpy `numpy.random.dirichlet([alpha]*n) `(*alpha* est 
un argument flottant à ajouter pour controller le niveau de non-iidness: plus alpha est petit, 
plus les différences dans le nombre d'images entre les clients sont grandes; *n* est le nombre de client total)

    **Réponse** :
    ```python
        props = np.random.dirichlet([0.8]*num_clients, np.random.seed(42))
        datasets = random_split(trainset, props, torch.Generator().manual_seed(42))
        print(len(datasets[0]), len(datasets[1]))
    ```

2. Retester votre code avec *alpha=0.8*
   1. Quelles sont les tailles des jeux de données pour chaque client ?
       
      **Réponse** : 11176 et 38824 
   
   2. Quelle est la précision finale du modèle ?
   
      **Réponse** : 61.11%, légèrement moins bon que le cas iid. 
   
3. Lancer votre code suiant: 
* Dans Terminal 1:
  ```bash
  > python server.py --rounds 10
  ```
* Dans Terminal 2:
  ```bash
  > python client.py --node-id 0 --data_split non_iid_class -local_epochs 1
  ```
* Dans Terminal 3:
  ```bash
  > python client.py --node-id 1 --data_split non_iid_class -local_epochs 1
  ```
  1. Comment les données sont distribués parmi les clients (indice: prepare_dataset.py) ?   
      **Réponse** : Chaque client possède un sous-ensemble de classes disjointes. Pour le cas où n=2, le client 0 a les classes allant de 0 à 4, tandis que le client 1 a les classes allant de 5 à 9.
  
  2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
      
     **Réponse** : Le modèle est nettement moins performant (avec une différence de plus de 10%). Les données sont bien plus non-iid (c'est-à-dire que le modèle local du client 0 diffère considérablement de celui du client 1). Après une période de calcul, les deux mises à jour sont déjà considérablement éloignées l'une de l'autre. La moyenne de ces deux mises à jour ne prend pas la bonne direction pour retrouver le modèle global.
  
4. Restez le code avec les même commandes sauf round=5, local_epochs=2.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** : Légèrement plus rapid. Le même temps pour les calculs mais moins de temps pour la communication. 
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** :Moins performant. Avec davantage de calculs locaux, les mises à jour des modèles 
   sont encore plus éloignées l'une de l'autre.

5. Restez le code avec les même commandes sauf round=2, local_epochs=5.
    1. Est-ce que l'execution est plus rapid ? Pourquoi ? 
        
       **Réponse** :  Légèrement plus rapid. Le même temps pour les calculs mais moins de temps pour la communication. 
    2. Quelle est votre observation sur le modèle appris par rapport aux résultats précédents ?
       **Réponse** : Moins performant. Avec davantage de calculs locaux, les mises à jour des modèles 
   sont encore plus éloignées l'une de l'autre.

6. Complétér le code avec l'autre jeu de données "CIFAR100" (fornie par torchvision)

       **Réponse** :
       ```python
       # Veuillez copie-coller votre changement de code ici avec le nom de fichier
      
        # N'oublie pas changer le modèle !!
       ```